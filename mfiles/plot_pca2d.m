## Copyright (C) 2018 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

% plot scaled PCA vectors
function h = plot_pca2d (P, l, meanxy)
  ab    = sqrt (l).'; % length ellipse axes
  theta = rad2deg (atan2 (P(2,1), P(1,1)));
  P     = P .* ab;     % pc scaled by length of ellipse axes

  h = addarrows (meanxy, P, 'r');
  set (h, 'linewidth', 3, 'maxheadsize', 0.2);
  set (h(2), 'color', 'm');

  holdst = ishold ();
  if ~holdst; hold on; end
  h(end+1) = drawEllipse ([meanxy ab theta], 'color', 'c', 'linewidth', 3);
  if ~holdst; hold off; end
end

