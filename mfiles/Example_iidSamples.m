%% SVD on independent and correlated samples
%
%%

% Copyright (C) 2018 Juan Pablo Carbajal
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.
%
% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
% Created: 2018-08-06

if exist ('OCTAVE_VERSION', 'builtin')
  pkg load statistics
endif

t  = 100;
n  = 5;
nr = 3;
feature_gen = [eye(nr); rand(n-nr, nr)];

%% IID samples
%
Xiid  = zscore ((feature_gen * randn (nr, t)).'); % column standarization

[U S V] = svd (Xiid, 1);
L = S * V.';
P = S * U.';

figure (1)
  subplot (2,1,1)
  plot (diag (S) / S(1,1), 'o-');
  axis tight
  grid on
  ylabel ('Singular values [iid samples]')

figure (2)
  subplot (2,1,1)
  plot(U(:,1:nr),'o-');
  axis tight
  ylabel ('U cols [iid samples]')

figure (3)
  subplot (2,1,1)
  plot(V(:,1:nr),'o-');
  axis tight
  ylabel ('V cols [iid samples]')

%% Correlated samples
%
tv = linspace (0,1,t).';
K = exp (-(tv - tv.').^2/2/0.2^2);
X = zscore (feature_gen * mvnrnd (0, K, nr)).'; % row standarization
K_ = cov (X.');
X = mvnrnd (0, K_, n).';

[U S V] = svd (X, 1);
L = S * V.';
P = S * U.';

figure (1)
  subplot (2,1,2)
  plot (diag(S)/S(1,1), 'o-');
  axis tight
  grid on
  ylabel ('Singular values [corr. samples]')
  xlabel ('Component #')
  
figure (2)
  subplot (2,1,2)
  plot(U(:,1:nr),'o-');
  axis tight
  ylabel ('U cols [corr. samples]')
  xlabel ('Sample #')

figure (3)
  subplot (2,1,2)
  plot(V(:,1:nr),'o-');
  axis tight
  ylabel ('V cols [corr. samples]')
  xlabel ('Element #')

