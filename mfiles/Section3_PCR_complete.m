
% -------------------------------------------------------------------------
% PCAR toolbox - Section3_PCR_complete.m
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2018.06.22
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

more off

% ------------------------------------------------------------
% Get data
% ------------------------------------------------------------
FileName        = fullfile('..', 'data', 'EawagData.csv')     ;
data            = csvread(FileName)   ;

% ------------------------------------------------------------
% Organize data into inputs X and outputs y
% ------------------------------------------------------------

X = data(:,1:end-1) ;
y = data(:,end) ;

% ------------------------------------------------------------
% Make some plots
% ------------------------------------------------------------

figure, 
  plot(X(1:5:end,:)')

figure, 
  plot(X(:,1),y,'k+')
  xlabel(['Abs 1 [%/m]'])
  ylabel(['NO_2^{-}-N [mg/L]'])
  Ylim=get(gca,'Ylim');
  Ylim(1)=0;
  set(gca,'Ylim',Ylim);
  Xlim=get(gca,'Xlim');
  Xlim(1)=0;
  set(gca,'Xlim',Xlim);
  
% ------------------------------------------------------------
% Calibrate PCA
% ------------------------------------------------------------

[T,par_mean,P,S,lambda]=calibratePCA(X,11) ;

figure, 
  bar(lambda(1:11)./sum(lambda))
  
figure, hold on,
  plot(T(:,1),y,'o')
  
figure, hold on,
  bar(P(:,1))
  
figure, hold on,
  plot(par_mean(:),'k.-')
  plot(par_mean(:)+P(:,1)*(-2000),'r.-')
  plot(par_mean(:)+P(:,1)*(+2000),'b.-')
  
x=T(:,1) ;
figure, hold on,
  plot(x,y,'o')
  beta_xy=calibrateLR(x,y);

figure, hold on,
    plot(x,y,'k+')
    plot(sort(x),sort(x)*beta_xy(2)+beta_xy(1),'b.:','linewidth',3)

yhat =((X-par_mean)*P(:,1))*beta_xy(2)+beta_xy(1);

figure, hold on,
  plot(y,yhat,'ko')
  plot(sort(y),sort(y),'k--')
  set(gca,'Xlim',[0 200],'Ylim',[0 200])
  
% ------------------------------------------------------------
% Leave-One-Out Cross-Validation for PCR
% ------------------------------------------------------------

maxPC = 42;
nSample =size(X,1);

for iSample=1:nSample
  
  disp(iSample)
  val = iSample;
  cal = setdiff(1:nSample,iSample);
  
  Xcal = X(cal,:);
  Xval = X(val,:);
  ycal = y(cal,:);
  yval = y(val,:);
  
  [Tcal,par_mean,P,S,lambda]=calibratePCA(Xcal,maxPC) ;

  for iPC=0:maxPC
      xcal = Tcal(:,1:iPC) ;
      beta_xy=calibrateLR(xcal,ycal);
      
      if iPC>0
        yval_pred =((Xval-par_mean)*P(:,1:iPC))*beta_xy((1:iPC)+1)+beta_xy(1);
      else
        yval_pred = beta_xy(1);
      end
      residual(val,iPC+1) = yval_pred-yval;
      
  end
  
end

MSRsd = std(residual.^2) ;

MSR = sum(residual.^2,1)/nSample;
RMSR = sqrt(MSR);

figure, hold on,
  bar(0:maxPC,MSR)
  plot(0:maxPC,MSR+MSRsd,'ro')
  xlabel('# Principal Components')
  ylabel('MSR [mg NO_2^--N/L]')
