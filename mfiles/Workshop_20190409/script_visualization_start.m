
% -------------------------------------------------------------------------
% PCAR toolbox - Section2_PCA_complete.m
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2018.06.22
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

% ========================================================================
% Get data
% ========================================================================

% ------------------------------------------------------------
%  Simulate flow 1 and 2
% ------------------------------------------------------------

m =500;
sigma = .5 ;
X(:,1) = 5+2*sin((1:m)'/m*20);
X(:,2) = 10+1*cos((1:m)'/m*50);

% ------------------------------------------------------------
%  Simulate flow 3, 4, and 5
% ------------------------------------------------------------
X(:,3) = X(:,1)+X(:,2)  ;
X(:,4) = X(:,3)         ;
X(:,5) = X(:,4)-X(:,2)  ;
Xtil = X+sigma*randn(m,5);

% ========================================================================
%   1. 2D case
% ========================================================================

figure, hold on 
  plot(Xtil(:,3:4),'.')
  legend({'Q_3','Q_4'})

% x = Xtil(:,3) ;
% y = Xtil(:,4) ;
%   
% beta_xy = calibrateLR (x, y);
% beta_yx = calibrateLR (y, x);

% figure, hold on 
% plot(x,y,'k.')
% plot(x,beta_xy(1)+beta_xy(2)*x,'k-', 'linewidth', 2)
% plot(beta_xy(1)+beta_xy(2)*y,y,'b-', 'linewidth', 2)
% plot([x x]',[y beta_xy(1)+beta_xy(2)*x]','k:', 'linewidth', 1)
% axis equal

% figure, hold on 
% plot(x,y,'ko')
% plot(x,beta_xy(1)+beta_xy(2)*x,'k-', 'linewidth', 2)
% plot(beta_xy(1)+beta_xy(2)*y,y,'b-', 'linewidth', 2)
% plot([beta_xy(1)+beta_xy(2)*y x]',[y y]','b:', 'linewidth', 1)
% axis equal

% pc = 1;
% [T, par_mean, P, S,lambda] = calibratePCA ([x y], pc);
% 
% x_recon = T(:,1) * P(1,1) + par_mean(1);
% y_recon = T(:,1) * P(2,1) + par_mean(2);
% 
% figure, hold on 
% plot(x,y,'k.')
% plot(x,beta_xy(1)+beta_xy(2)*x,'k-', 'linewidth', 2)
% plot(beta_xy(1)+beta_xy(2)*y,y,'b-', 'linewidth', 2)
% plot(x_recon,y_recon,'r-', 'linewidth', 2)
% plot([x x_recon]',[y y_recon]','r:', 'linewidth', 1)
% axis equal
% 
% 
% pc = 2;
% [T, par_mean, P, S,lambda] = calibratePCA ([x y], pc);
% 
% 
% figure, hold on 
% plot(x,y,'k.')
% plot(par_mean(1),par_mean(2),'mo','MarkerFaceColor','m')
% h=addarrows(par_mean,P(:,1),'r'); set(h,'linewidth',2)
% h=addarrows(par_mean,P(:,2),'b'); set(h,'linewidth',2)
% axis equal
% 
% 
% figure, hold on 
% plot(x,y,'k.')
% plot(par_mean(1),par_mean(2),'mo','MarkerFaceColor','m')
% h=addarrows(par_mean,P(:,1)*(lambda(1)).^(1/2),'r'); set(h,'linewidth',2)
% h=addarrows(par_mean,P(:,2)*(lambda(2)).^(1/2),'b'); set(h,'linewidth',2)
% axis equal
% 
% figure, hold 
% bar(lambda/sum(lambda)*100)    
% xlabel('PC')
% ylabel('% variance')


% ========================================================================
%   2. 3D case
% ========================================================================

% ------------------------------------------------------------
% Make some plots
% ------------------------------------------------------------

% figure, hold on 
%   plot(Xtil(:,1:3),'o')
%   legend({'Q_1','Q_2','Q_3'})
% 
% figure, hold on 
%   plot3(Xtil(:,1),Xtil(:,2),Xtil(:,3),'ko')
%   xlabel('Q_1')
%   ylabel('Q_2')
%   zlabel('Q_3')
  
% ------------------------------------------------------------
% SVD-based PCA
% ------------------------------------------------------------

% [T,par_mean,P,S,lambda]=calibratePCA(Xtil(:,1:3),3) ;

% ------------------------------------------------------------
% Vizualize model
% ------------------------------------------------------------

% % -> in original space - new coordinate system
% 
%   h=addarrows(par_mean,P(:,1)*(mean(lambda)).^(1/2),'m'); set(h,'linewidth',2)
%   h=addarrows(par_mean,P(:,2)*(mean(lambda)).^(1/2),'r'); set(h,'linewidth',2)
%   h=addarrows(par_mean,P(:,3)*(mean(lambda)).^(1/2),'b'); set(h,'linewidth',2)
%   axis equal
%   xlabel('Q_1')
%   ylabel('Q_2')
%   zlabel('Q_3')
%   
% % -> in original space - scale proportional to standard deviation 
% figure, hold on 
%   plot3(Xtil(:,1),Xtil(:,2),Xtil(:,3),'ko')
%   h=addarrows(par_mean,P(:,1)*lambda(1).^(1/2),'m'); set(h,'linewidth',2)
%   h=addarrows(par_mean,P(:,2)*lambda(2).^(1/2),'r'); set(h,'linewidth',2)
%   h=addarrows(par_mean,P(:,3)*lambda(3).^(1/2),'b'); set(h,'linewidth',2)
%   axis equal
%   xlabel('Q_1')
%   ylabel('Q_2')
%   zlabel('Q_3')
% 
%  % -> dimension reduction from 3D to 2D
% figure, hold on 
%   plot(T(:,1),T(:,2),'ko')
%   h=addarrows(zeros(2,1),[1 0]'*lambda(1).^(1/2),'m'); set(h,'linewidth',2)
%   h=addarrows(zeros(2,1),[0 1]'*lambda(2).^(1/2),'r'); set(h,'linewidth',2)
%   axis equal
%   xlabel('PC1')
%   ylabel('PC2')
%   
% 
% % introducing eigenvectors
% figure, hold on 
%   bar(P(:,1))
% 
% figure, hold on 
%   bar(P(:,2))
  
% ------------------------------------------------------------
% Selecting dimension
% ------------------------------------------------------------

% figure, hold on,
%   bar(1:length(lambda),lambda./sum(lambda)*100)
%   set(gca,'Xtick',1:length(lambda))
%   xlabel('principal components')
%   ylabel('% variance')
%   
% figure, hold on,
%   bar(1:length(lambda),cumsum(lambda)./sum(lambda)*100)
%   set(gca,'Xtick',0:length(lambda))
%   xlabel('# principal components')
%   ylabel('% cumulative variance')
  
% ------------------------------------------------------------
%  Interpreting model: Simulate a sample
% ------------------------------------------------------------

% Tsim1 = [ 4 0 ];
% Tsim2 = [ 0 4 ];
% 
%  % -> dimension reduction from 3D to 2D
% figure, hold on 
%   plot(T(:,1),T(:,2),'ko')
%   plot(Tsim1(:,1),Tsim1(:,2),'ko','MarkerFaceColor','m')
%   plot(Tsim2(:,1),Tsim2(:,2),'ko','MarkerFaceColor','r')
%   h=addarrows(zeros(2,1),[1 0]'*lambda(1).^(1/2),'m'); set(h,'linewidth',2)
%   h=addarrows(zeros(2,1),[0 1]'*lambda(2).^(1/2),'r'); set(h,'linewidth',2)
%   xlabel('PC1')
%   ylabel('PC2')
%   axis equal
%   
% Xsim1 = par_mean+Tsim1*P(:,1:pc)';
% Xsim2 = par_mean+Tsim2*P(:,1:pc)';
% 
% % -> in original space 
% figure, hold on 
%   plot3(Xtil(:,1),Xtil(:,2),Xtil(:,3),'ko')
%   plot3(Xsim1(:,1),Xsim1(:,2),Xsim1(:,3),'ko','MarkerFaceColor','m')
%   plot3(Xsim2(:,1),Xsim2(:,2),Xsim2(:,3),'ko','MarkerFaceColor','r')
%   h=addarrows(par_mean,P(:,1)*lambda(1).^(1/2),'m'); set(h,'linewidth',2)
%   h=addarrows(par_mean,P(:,2)*lambda(2).^(1/2),'r'); set(h,'linewidth',2)
%   h=addarrows(par_mean,P(:,3)*lambda(3).^(1/2),'b'); set(h,'linewidth',2)
%   xlabel('Q_1')
%   ylabel('Q_2')
%   zlabel('Q_3')
%   axis equal
  
% ========================================================================
%   3. 5D case
% ========================================================================

% ------------------------------------------------------------
% Make some plots
% ------------------------------------------------------------

% figure, hold on
%   plot(Xtil(:,1:3),'o')
%   plot(Xtil(:,4:5),'x')
%   set(gca,'Ylim',[0 42])
%   legend({'Q_1','Q_2','Q_3','Q_4','Q_5'})

% ------------------------------------------------------------
% SVD-based PCA
% ------------------------------------------------------------

% [T,par_mean,P,S,lambda]=calibratePCA(Xtil,5) ;

  
% ------------------------------------------------------------
% Selecting dimension
% ------------------------------------------------------------

% figure, hold on,
%   bar(1:length(lambda),[ lambda ])
%   set(gca,'Xtick',1:length(lambda))
%   
% lambda0=[0 ; lambda];
% figure, hold on,
%   bar(0:length(lambda),cumsum(lambda0)./sum(lambda0))
%   set(gca,'Xtick',0:length(lambda))
%   
%   
% figure, hold on 
%   plot(T(:,1),T(:,2),'ko')
%   h=addarrows(zeros(2,1),[1 0]'*lambda(1).^(1/2),'m'); set(h,'linewidth',2)
%   h=addarrows(zeros(2,1),[0 1]'*lambda(2).^(1/2),'r'); set(h,'linewidth',2)
%   axis equal
%   
% 
% % introducing eigenvectors
% figure, hold on 
%   bar(P(:,1))
% 
% figure, hold on 
%   bar(P(:,2))
%   
%   
% figure, hold on 
%   scatter(T(:,1),T(:,2),11,Xtil(:,1),'filled')
%   
% figure, hold on 
%   scatter(T(:,1),T(:,2),11,Xtil(:,2),'filled')
%   
  
disp('Script completed')
