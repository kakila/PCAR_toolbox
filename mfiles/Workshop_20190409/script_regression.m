clc
clear all
close all

addpath('..')

% ========================================================================
%   Load data
% ========================================================================

filename	=	'../../data/Eawag_dataset2.csv' ;

tabData     =	readtable(filename)     ;
aData       =   table2array(tabData)    ;
cNO2        =	aData(:,3)              ;
cNO3        =	aData(:,4)              ;
spectra     =	aData(:,5:end)          ;
clear aData
clear tabData

[~,ix1,iy1]=unique([cNO2]);
[~,ix2,iy2]=unique([cNO3]);
iCal = find(mod(iy1+iy2,2)==0) ;
iVal = setdiff(1:size(cNO2,1),iCal) ;

figure, hold on
plot(cNO2(:,:),cNO3(:,:),'ko','MarkerFaceColor','k')
idx1=and(iy1==min(iy1),iy2==max(iy2));
idx2=and(iy1==max(iy1),iy2==min(iy2));
plot(cNO2(idx1),cNO3(idx1),'ko','MarkerFaceColor','c')
plot(cNO2(idx2),cNO3(idx2),'ko','MarkerFaceColor','r')
xlabel('mg NO_2^-N/L')
ylabel('mg NO_3^-N/L')

figure, hold on
plot(spectra(:,:)','k-')
plot(spectra(idx1,:)','c-')
plot(spectra(idx2,:)','r-')


figure, hold on
plot(cNO2(iCal,:),cNO3(iCal,:),'ko','MarkerFaceColor','b')
plot(cNO2(iVal,:),cNO3(iVal,:),'ko','MarkerFaceColor','r')
xlabel('mg NO_2^-N/L')
ylabel('mg NO_3^-N/L')

return

% ========================================================================
%   Regression with two inputs - nitrate (NO3)
% ========================================================================

X = spectra(:,[18]);
y = cNO3 ;

% calibration
X1cal = [ ones(length(iCal),1) X(iCal,:) ];
ycal = y(iCal,:);

beta = (X1cal'*X1cal)^(-1)*X1cal'*ycal ;
ycal_hat = X1cal*beta;

% validation
X1val = [ ones(length(iVal),1) X(iVal,:) ];
yval = y(iVal,:);
yval_hat = X1val*beta;

% display
figure, hold on
    plot(ycal,ycal_hat,'b+')
    plot(yval,yval_hat,'r+')
    plot([-25 300],[-25 300],'k-')
    xlabel('mg NO_3^-N/L (lab)')
    ylabel('mg NO_3^-N/L (prediction)')
    title('Linear regression NO_3 - 2 inputs')
    
% ========================================================================
%   Regression with all inputs
% ========================================================================

X = spectra(:,:);

% calibration
X1cal = [ ones(length(iCal),1) X(iCal,:) ];
ycal = y(iCal,:);

beta = (X1cal'*X1cal)^(-1)*X1cal'*ycal ;
ycal_hat = X1cal*beta;

% validation
X1val = [ ones(length(iVal),1) X(iVal,:) ];
yval = y(iVal,:);
yval_hat = X1val*beta;

% display
figure, hold on
    plot(ycal,ycal_hat,'b+')
    plot(yval,yval_hat,'r+')
    plot([-25 300],[-25 300],'k-')
    xlabel('mg NO_3^-N/L (lab)')
    ylabel('mg NO_3^-N/L (prediction)')
    title('Linear regression NO_3 - all inputs')

    return
    
% ========================================================================
%   Regression with pca
% ========================================================================

pc = 2;

% calibration
[Tcal, par_mean, P, S, lambda] = calibratePCA (X(iCal,:), pc) ;
T1cal = [ ones(length(iCal),1) Tcal ];
ycal = y(iCal,:);

gamma = (T1cal'*T1cal)^(-1)*T1cal'*ycal ;
ycal_hat = T1cal*gamma;

% validation
Tval = applyPCA(X(iVal,:), par_mean, P) ;
T1val = [ ones(length(iVal),1) Tval ];
yval = y(iVal,:);
yval_hat = T1val*gamma;

% display
figure, hold on
    plot(ycal,ycal_hat,'b+')
    plot(yval,yval_hat,'r+')
    plot([-25 300],[-25 300],'k-')
    xlabel('mg NO_3^-N/L (lab)')
    ylabel('mg NO_3^-N/L (prediction)')
    title('Linear regression NO_3 - 2 principal scores')

    
% ========================================================================
%   Interpreting PCA model
% ========================================================================

% showing eigenvectors
figure, hold on 
  bar(P(:,1))

figure, hold on 
  bar(P(:,2))
  
figure, hold on 
  plot(Tcal(:,1),Tcal(:,2),'ko')
  h=addarrows(zeros(2,1),[1 0]'*lambda(1).^(1/2),'m'); set(h,'linewidth',2)
  h=addarrows(zeros(2,1),[0 1]'*lambda(2).^(1/2),'r'); set(h,'linewidth',2)
  axis equal
  
% ========================================================================
%   Interpreting PCR model
% ========================================================================
figure, hold on 
  bar(P(:,1:2)*gamma(2:3))

  
% ========================================================================
%   Leave-One-Out Cross-Validation to select the number of PCs
% ========================================================================

% --- cross-validation loop ---
N = size(X,1) ;
pcmax =42;
Residual = nan(N,pcmax) ;

for i=1:N
    iVal = i ;
    iCal = setdiff(1:N,iVal) ;
    
    disp(iVal)
    
    [Tcal, par_mean, P, S, lambda] = calibratePCA (X(iCal,:), pcmax) ;
    Tval = applyPCA(X(iVal,:), par_mean, P) ;
    
    for pc=1:pcmax
        
        % calibration
        T1cal = [ ones(length(iCal),1) Tcal(:,1:pc) ];
        ycal = y(iCal,:);
        
        gamma = (T1cal'*T1cal)^(-1)*T1cal'*ycal ;
        ycal_hat = T1cal*gamma;
        
        % validation
        Tval = applyPCA(X(iVal,:), par_mean, P) ;
        T1val = [ ones(length(iVal),1) Tval(:,1:pc) ];
        yval = y(iVal,:);
        yval_hat = T1val*gamma;
        
        Residual(iVal,pc) = yval_hat-yval;
    end
end

% --- selection mechanism ---
Residual2 = Residual.^2 ;
MSR =  mean(Residual2);
MSRstd = std(Residual2);
RMSR = sqrt(MSR) ;
RMSRplusOneStDev = sqrt(MSR+MSRstd) ;

figure, hold on
plot(1:pcmax,RMSR,'ko')
plot(1:pcmax,RMSRplusOneStDev,'k+')

[RMSRmin,pcmin]=min(RMSR);

plot([0 pcmax],[ RMSRplusOneStDev(pcmin) RMSRplusOneStDev(pcmin)],'r-')
plot([pcmin pcmin],[ RMSRmin RMSRplusOneStDev(pcmin)],'k-')
plot(pcmin,RMSRmin,'ro','MarkerFaceColor','c')


  
disp('Script completed')


return