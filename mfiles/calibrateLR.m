% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  

% Author: Kris Villez <Kris.Villez@eawag.ch>
% Last version: 2017.03.10

function beta = calibrateLR (T, Y)
% BETA = CALIBRATELR (X, Y)
% Perform linear regression Y = beta * [1 X], with 
% X n-by-d array and Y n-by-1.
% 
% The vector of parameters BETA contains the intercept in the first component 
% and the slopes in the remaining. In the d=1 case, this function is equivalent 
% to fliplr(polyfit(X, Y, 1)).
%
% Example:
%
%     X = rand(10, 3);
%     y = X * [-1; 2; 1] + 0.1 * randn (10,1);
%     beta = calibrateLR (X, y)
%

  [mx, nx] = size (T) ;
  [my, ny] = size (Y) ;
  assert (mx == my, 'calibrateLR.m : Row dimensions do not match')

  T1   = [ones(mx, 1) T]           ;
  beta = (T1' * T1)^(-1) * T1' * Y ; %FIXME: use cholinv?

end

%!demo
%! X = rand(10,3);
%! y = X * [-1; 2; 1] + 0.1 * randn (10,1);
%! beta = calibrateLR (X, y)

