% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  

% Author: Kris Villez <Kris.Villez@eawag.ch>
% Last version: 2018.06.27

function [T, par_mean, P, S, lambda] = calibratePCA (X, pc)
% Computes Principal Components Analysis using the Singular Value Decomposition
%
%  The data matrix is centered before computing the PCA by removing the
%  column mean of the data.
%
%  [T, MEAN, P] = CALIBRATEPCA(X, PC)
%     Returns the scores (T) of the principal components (P) of the data (X).
%     The column mean of the data is returned in MEAN.
%
%  [T, MEAN, P, S, L] = CALIBRATEPCA(X, PC)
%     Aditionally return the Singular Values matrix (S) and the eigenvalues (L) 
%     of the covariance matrix of the data.
%
%  Example:
%
%      x = linspace (-1, 1, 10).' + 0.1 * randn (10, 1);
%      y = 2 * x + 0.3 * randn (10, 1);
%      xy = zscore ([x y]);
%      [T, mean_xy, P] = calibratePCA (xy, 2);
%      plot (xy(:,1), xy(:,2), 'o');
%      hold on
%      addarrows (mean_xy, P(:,1), 'r');
%      addarrows (mean_xy, P(:,2), 'b');
%      axis tight
%      hold off
%
  [mx, nx] = size (X) ;
  maxpc    = min (mx, nx) ;

  % Column-wise mean centering
  par_mean = mean (X, 1)                    ;
  Xms      = X - repmat (par_mean, [mx 1])  ;

  % Singular Value Decomposition (SVD)
  [U, S, P] = svd (Xms, 'econ')             ;
  T         = U * S                         ; % Loadings X = T * P.'

  % Eigenvalues of the covariance matrix:
  lambda = diag (S).^2 / mx ;

  % Component selection
  T = T(:, 1:pc) ;
  P = P(:, 1:pc) ;

end

%!demo
%! x = linspace (-1, 1, 10).' + 0.1 * randn (10, 1);
%! y = 2 * x + 0.3 * randn (10, 1);
%! xy = zscore ([x y]);
%! [T, mean_xy, P] = calibratePCA ([x y], 2);
%! plot (x, y, 'o');
%! hold on
%! addarrows (mean_xy, P(:,1), 'r');
%! addarrows (mean_xy, P(:,2), 'b');
%! axis equal
%! hold off

