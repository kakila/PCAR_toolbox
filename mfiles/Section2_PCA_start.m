
% -------------------------------------------------------------------------
% PCAR toolbox - Section2_PCA_complete.m
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2018.06.22
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2017 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

% ========================================================================
% Get data
% ========================================================================

% ------------------------------------------------------------
%  Simulate flow 1 and 2
% ------------------------------------------------------------

m =100;
X(:,1) = 10+5*randn(m,1);
X(:,2) = 20+3*randn(m,1);

% ------------------------------------------------------------
%  Simulate flow 3, 4, and 5
% ------------------------------------------------------------
X(:,3) = X(:,1)+X(:,2)  ;
X(:,4) = X(:,3)         ;
X(:,5) = X(:,4)-X(:,2)  ;
Xtil = X+randn(100,5)*.5;

% ========================================================================
%   1. 3D case
% ========================================================================

% ------------------------------------------------------------
% Make some plots
% ------------------------------------------------------------

figure, hold on 
  plot(Xtil(:,1:3),'o')
  set(gca,'Ylim',[0 42])
  legend({'Q_1','Q_2','Q_3'})
