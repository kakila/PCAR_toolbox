% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  

% Author: Kris Villez <Kris.Villez@eawag.ch>
% Last version: 2018.06.22

function T = applyPCA(X, par_mean, P)
% Computes Principal Components Analysis using the Singular Value Decomposition
%
%  The data matrix is centered before computing the PCA by removing the
%  column mean of the data.
%
%  T = applyPCA(X, par_mean, P)
%     Returns the scores (T) of the principal components (P) of the data (X) 
%     given a model specified with a mean vector and P.
%

  [mx, nx] = size (X) ;

  % Column-wise mean centering
  Xm       = X - repmat (par_mean, [mx 1]) ; 
  %TODO: matlabhas now broadcasting
  % this works:
  % X - par_mean
  T        = Xm * P                        ;

end

