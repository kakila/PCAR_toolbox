
% -------------------------------------------------------------------------
% PCAR toolbox - Section1_PCA_complete.m
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2018.06.22
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

% ========================================================================
% Get data
% ========================================================================

FileName        = fullfile('..', 'data', 'EawagData.csv')     ;
disp('Reading file')
data            = dlmread(FileName)   ;
disp('Done reading file')

% ------------------------------------------------------------
% Organize data into inputs X and outputs y
% ------------------------------------------------------------

X = data(:,1:end-1) ;
y = data(:,end) ;

% ------------------------------------------------------------
% Make some plots
% ------------------------------------------------------------

figure, 
  plot(X(1:5:end,:)')

figure, 
  plot(X(:,1),y,'k+')
  xlabel(['Abs 1 [%/m]'])
  ylabel(['NO_2^{-}-N [mg/L]'])
  Ylim=get(gca,'Ylim');
  Ylim(1)=0;
  set(gca,'Ylim',Ylim);
  Xlim=get(gca,'Xlim');
  Xlim(1)=0;
  set(gca,'Xlim',Xlim);
  
