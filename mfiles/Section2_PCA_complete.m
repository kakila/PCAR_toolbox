
% -------------------------------------------------------------------------
% PCAR toolbox - Section2_PCA_complete.m
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2018.06.22
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

% ========================================================================
% Get data
% ========================================================================

% ------------------------------------------------------------
%  Simulate flow 1 and 2
% ------------------------------------------------------------

m =100;
X(:,1) = 10+5*randn(m,1);
X(:,2) = 20+3*randn(m,1);

% ------------------------------------------------------------
%  Simulate flow 3, 4, and 5
% ------------------------------------------------------------
X(:,3) = X(:,1)+X(:,2)  ;
X(:,4) = X(:,3)         ;
X(:,5) = X(:,4)-X(:,2)  ;
Xtil = X+randn(100,5)*.5;

% ========================================================================
%   1. 3D case
% ========================================================================

% ------------------------------------------------------------
% Make some plots
% ------------------------------------------------------------

figure, hold on 
  plot(Xtil(:,1:3),'o')
  set(gca,'Ylim',[0 42])
  legend({'Q_1','Q_2','Q_3'})

figure, hold on 
  plot3(Xtil(:,1),Xtil(:,2),Xtil(:,3),'ko')
  
% ------------------------------------------------------------
% SVD-based PCA
% ------------------------------------------------------------

[T,par_mean,P,S,lambda]=calibratePCA(Xtil(:,1:3),3) ;

% ------------------------------------------------------------
% Vizualize model
% ------------------------------------------------------------

% -> in original space - new coordinate system

  addarrows(par_mean,P(:,1)*(mean(lambda)).^(1/2),'m')
  addarrows(par_mean,P(:,2)*(mean(lambda)).^(1/2),'r')
  addarrows(par_mean,P(:,3)*(mean(lambda)).^(1/2),'b')
  axis equal
  
% -> in original space - scale proportional to standard deviation 
figure, hold on 
  plot3(Xtil(:,1),Xtil(:,2),Xtil(:,3),'ko')
  addarrows(par_mean,P(:,1)*lambda(1).^(1/2),'m')
  addarrows(par_mean,P(:,2)*lambda(2).^(1/2),'r')
  addarrows(par_mean,P(:,3)*lambda(3).^(1/2),'b')
  axis equal

 % -> dimension reduction from 3D to 2D
figure, hold on 
  plot(T(:,1),T(:,2),'ko')
  addarrows(zeros(2,1),[1 0]'*lambda(1).^(1/2),'m')
  addarrows(zeros(2,1),[0 1]'*lambda(2).^(1/2),'r')
  axis equal
  
% introducing eigenvectors
figure, hold on 
  bar(P(:,1))

figure, hold on 
  bar(P(:,2))
  

% ------------------------------------------------------------
% Selecting dimension
% ------------------------------------------------------------

figure, hold on,
  bar(1:length(lambda),lambda)
  set(gca,'Xtick',1:length(lambda))
  
lambda0=[0 ; lambda];
figure, hold on,
  bar(0:length(lambda),cumsum(lambda0)./sum(lambda0))
  set(gca,'Xtick',0:length(lambda))
    
% ========================================================================
%   1. 5D case
% ========================================================================

% ------------------------------------------------------------
% Make some plots
% ------------------------------------------------------------

figure, hold on
  plot(Xtil(:,1:3),'o')
  plot(Xtil(:,4:5),'x')
  set(gca,'Ylim',[0 42])
  legend({'Q_1','Q_2','Q_3','Q_4','Q_5'})

% ------------------------------------------------------------
% SVD-based PCA
% ------------------------------------------------------------

[T,par_mean,P,S,lambda]=calibratePCA(Xtil,5) ;

  
% ------------------------------------------------------------
% Selecting dimension
% ------------------------------------------------------------

figure, hold on,
  bar(1:length(lambda),[ lambda var(T,1)'])
  set(gca,'Xtick',1:length(lambda))
  
lambda0=[0 ; lambda];
figure, hold on,
  bar(0:length(lambda),cumsum(lambda0)./sum(lambda0))
  set(gca,'Xtick',0:length(lambda))
  
  
figure, hold on 
  plot(T(:,1),T(:,2),'ko')
  addarrows(zeros(2,1),[1 0]'*lambda(1).^(1/2),'m')
  addarrows(zeros(2,1),[0 1]'*lambda(2).^(1/2),'r')
  axis equal
  

% introducing eigenvectors
figure, hold on 
  bar(P(:,1))

figure, hold on 
  bar(P(:,2))
  