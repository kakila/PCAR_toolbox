function [V_hat,lambda2_hat]=fPCAconstruct(lambda2,A,V)

% construct PCA model
LVmax       =   length(lambda2) ;	% maximal number of PCs
lambda2_hat	=   lambda2(1:A)    ;	% selected eigenvalues
V_hat       =	V(:,1:A)        ;   % selected eigenvectors