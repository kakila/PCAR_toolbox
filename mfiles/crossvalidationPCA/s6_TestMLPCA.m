clc
clear all
close all
warning('off','stats:factoran:ZeroVariance')
warning('off','stats:factoran:ZeroVarianceSignificance')
warning('off','stats:factoran:NoSignificance')
addpath('C:\A\Research\Code\Git')
%rng(73)

format short

addpath('C:\A\Research\Code\Tools\Extern\ppca\ppca')
addpath('C:\A\Research\Code\Tools\Extern\ipca_basic_30042018');
seed=[ 73] ;

opts = statset('factoran')   ;
opts.MaxFunEvals = inf;
opts.MaxIter = inf;

for CaseNumber =1:18
    
    %s1_SimulateData
    % [Lambda,Psi,o,stat]=factoran(Sigma2,nLV,'Xtype','covariance','Rotate','none','start','random');
    % psi_true = Psi.^(1/2) ;
    
    dataset=2;
    switch dataset
        case 1
            %   rng(seed)
            nSample = 1000;
            G = [1 0 1 1 1 ; 0 1 1 1 0 ];
            G(2,:) = G(2,:)*2 ;
            u1 =  1*randn(nSample,1);
            u2 =  1*randn(nSample,1);
            F = [u1 u2]*G ;
            psi_true = [0.1 0.08 0.15 .2 .18] ;
            
            Sigma2 = G'*G+diag(psi_true.^2);
            
            Ytil = F+randn(nSample,5)*diag(psi_true);
            nLV = 2;
        case 2
            Ytil = dlmread('../data/simdata.csv',';');
        case 3
            load('../../Y31')
            Ytil =Y3(1:400:end,1:10:end);
            clear Y3;
    end
    
    %     [P,S,~]=svd(Sigma2*5);
    %     Y = diag(diag(S).^(1/2))*P';
    %
    %
    %
    %     [W, mu, psi, llh] = fa([Y ; -Y]', nLV);
    %
    %     CovYhat                   =	W*W'+diag(psi)    ;
    %
    %     neglogpdf1                  =	trace(inv(CovYhat)*Sigma2) + log(det(CovYhat))   ;
    %     neglogpdf1
    %
    %     D2 = diag(Sigma2) ;
    %     DD = (D2*D2').^(1/2);
    %     [L_s,Psi_s,o,stat]=factoran([Y ; -Y],nLV,'Xtype','data','Rotate','none','start','random','optimopts',opts);
    %     Psi = diag((diag(Psi_s)).*(DD))  ;
    %
    %     CovYhat_s                   =	L_s*L_s'+diag(Psi_s)    ;
    %     CovYhat                     =	CovYhat_s.*(DD)       ;
    %     LL = L_s*L_s'.*(DD) ;
    %     Psi = diag(Psi_s).*(DD) ;
    %     Psi1 = Psi;
    %     CovYhat_s                   =	LL+Psi    ;
    %     LL+Psi
    %     LL1 = LL;
    %     neglogpdf1                  =	trace(inv(LL+Psi)*Sigma2) + log(det(LL+Psi))   ;
    %     neglogpdf1
    %
    %     return
    %     Sigma2
    %
    %     return
    %     [P,sval, v, Amat, Qe, errflag] = ipca([Y ; -Y]',nLV,[]);
    %     (P*diag(sval.^2)*P').*(diag(Qe)*diag(Qe)').^(1/2)
    %     LL = (P(:,1:2)*diag(sval(1:2).^2-1)*P(:,1:2)').*(diag(Qe)*diag(Qe)').^(1/2) ;
    %     LL2 = LL;
    %     Psi = +eye(5).*(diag(Qe)*diag(Qe)').^(1/2) ;
    %     Psi2 = Psi;
    %     trace(inv(LL+Psi)*Sigma2)
    %     neglogpdf1                  =	trace(inv(LL+Psi)*Sigma2) + log(det(LL+Psi))   ;
    %     neglogpdf1
    %     return
    %     cov(A,0)
    %     cov(A',0)
    %     pause
    %
    %     psi_true1 = Psi.^(1/2) ;
    %
    %     psi_true(:)'
    %     psi_true1(:)'
    %
    %
    %     pause
    
    [m,n]=size(Ytil);
    
    center	=	sum(Ytil,1)/m       ;
    Yc      =	Ytil-center         ;
    
    S	=	(Yc'*Yc)/m	;
    
    clear Ytil
    
    %figure, surf(CovYtil)
    CovYhat	=   S         ;
    neglogpdf0 = trace(inv(CovYhat)*S) + log(det(CovYhat));
    neglogpdf0
    
    k = 0 ;
    while ( (n-k).^2 - n -k )>=0
        k = k+1;
    end
    nMaxFactor = min(11,k-1);
    
    
    for nFactor = 0:nMaxFactor
        
        disp([ num2str(CaseNumber) ' - ' num2str(nFactor)])
        
        % ====================================================================
        %   Exploratory Factory Analysis
        % ====================================================================
        disp('	EFA')
        
        tic
        D2 = diag(S) ;
        D = D2.^(1/2);
        %DD = (D*D') ;
        %DD = DD-diag(diag(DD))+diag((D2)) ;
        DD = (D2*D2').^(1/2);
        for vers=1:2
            switch vers
                case 1
                    [L_s,Psi_s,R,stat]=factoran(S./DD,nFactor,'Xtype','covariance','Rotate','none','start','Rsquared','optimopts',opts);
                    
                    % 1. construct covariance matrix with EFA form:
                    
                    % 1.1. scaled variation
                    CovYhat_s                   =	L_s*L_s'+diag(Psi_s)    ;
                    CovYhat                     =	CovYhat_s.*(D*D')       ;
                    neglogpdf1                  =	trace(inv(CovYhat)*S) + log(det(CovYhat))   ;
                    neglogpdf(nFactor+1,vers)	=	neglogpdf1                                  ;
                    
                    % 1.2. undo scaling
                    Psi = diag((diag(Psi_s)).*(D*D'))  ;
                    LL = (L_s*L_s').*(D*D')  ;
                    CovYhat = LL+diag(Psi) ;
                    [P,Lambda,~] = svd(LL);
                    L = P*diag( diag( Lambda ).^(1/2) ) ;
                    CovYhat = L*L'+diag(Psi)    ;
                    
                case 2
                    [L, mu, Psi, llh] = fa(Yc', nFactor);
                    
            end
            
            time2=toc;
            % 2. construct covariance matrix as scaled PPCA model
            % CovYhat = L*L'+diag(Psi)    ;
            %        = (L*L'+diag(Psi))./(psi*psi').*(psi*psi')    ;
            psi     =	Psi.^(1/2)                                  ;
            Ss = (L*L'+diag(Psi))./(psi*psi') ;
            CovYhat =	Ss.*(psi*psi')    ;
            [P,Lambda,~]=svd(Ss);
            CovYhat =	(P*Lambda*P').*(psi*psi')    ;
            CovYhat =	diag(psi)*(P*Lambda*P')*diag(psi)    ;
            lambda_efa = diag(Lambda) ;
            %CovYhat
            psi_efa = psi;
            neglogpdf1                  =	trace(inv(CovYhat)*S) + log(det(CovYhat))   ;
            neglogpdf(nFactor+1,vers)	=	neglogpdf1                                  ;
                    
        end
        
        %     par_init = [ Psi ; vec(L(:,1:nFactor)) ];
        %     par_fin = fminsearch( @(par) neglogfun(par,S), par_init);
        %
        %     n =size(S,1);
        %     nFactor = (length(par_fin)/n-1) ;
        %     Psi = par_fin(1:n) ;
        %     L = reshape(par_fin((n+1):end),[n nFactor]);
        %     CovYhat = L*L'+diag(Psi) ;
        %
        %     neglogpdf1                  =	trace(inv(CovYhat)*S) + log(det(CovYhat))   ;
        %     neglogpdf(nFactor+1,2)	=	neglogpdf1                                  ;
        
        % ====================================================================
        %   Iterative Principal Component Analysis
        % ====================================================================
        disp('	IPCA')
        [P,sval, v, Amat, Qe, errflag] = ipca(Yc',nFactor,[]);
        sval( (nFactor+1):end ) = 1;
        lambda_ipca = sval.^2 ;
        Lambda = diag(lambda_ipca);
        Psi = ((diag(Qe)*diag(Qe)').^(1/2)) ;
        CovYhat = (P*Lambda*P').*Psi ;
        
        neglogpdf1                  =	trace(inv(CovYhat)*S) + log(det(CovYhat))   ;
        neglogpdf(nFactor+1,3)	=	neglogpdf1                                  ;
        
        psi_ipca = diag(Psi).^(1/2);
        
%         figure(100+nFactor),
%         subplot(2,1,1),
%         bar([psi_efa psi_ipca  ])
%         subplot(2,1,2),
%         bar([lambda_efa lambda_ipca ])
%     drawnow()
        
    end
    
    figure, hold on,
    plot(0:nMaxFactor,squeeze(neglogpdf(:,1)),'k+-')
    plot(0:nMaxFactor,squeeze(neglogpdf(:,2)),'kx-')
    plot(0:nMaxFactor,squeeze(neglogpdf(:,3)),'ko-')
    legend({'EFA1','EFA2','IPCA'})
    drawnow()
    pause(1)
    %pause(11)
    
end

return
