clc
% clear all
% close all

%   DEFINE DATA TYPE
SetName     =	'E'    ;   % use any of the following options: 'A','B1','B2','C','D','E'
CaseNumber  =   5       ;   % number between 1 and 25 (A/D/E), 18 (B1/B2), and 24 (C)
nSample     =   1000	;

%   PRELIMINARIES
%   Set random number generator seed: produces the exact same noise
%   sequence - useful to compare methods 
seed = 42 ;
rng(seed) 
more off

%   GET COVARIANCE MATRIX AND META-DATA
[Sigma2,nOV,nLV] = fGenerateCovariance(SetName,CaseNumber) ; % currently complete: A,B1,B2,C

% GENERATE DATA
switch SetName
    case 'D'
        % NON-GAUSSIAN DIST - NOT IMPLEMENTED YET
        error('Set D not implemented yet')
    otherwise
        W = randn(nSample,nOV) ;
end

Q     =   chol(Sigma2,'lower')  ;
Ytil  =   W(:,1:nOV)*Q'         ;

% SAVE DATA
dlmwrite('../../data/simdata.csv',Ytil,'delimiter',';');
dlmwrite('../../data/trueLV.csv',nLV,'delimiter',';');

for f=0:4
    [Lambda,Psi,o,stat]=factoran(Sigma2,f,'Xtype','covariance','Rotate','none');
    loglike(f+1) = stat.loglike ;
end

% figure, 
%     bar(0:f,-loglike)
% 
% figure, 
%     bar(eigs(Sigma2,nOV))