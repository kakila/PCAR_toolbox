function error2  = fPCAerror2(Y,mu,sigma,lambda2,V,nOV,colM)

% compute squared residual error
lambda	=	lambda2.^(1/2)	;

if (nargin >=7 && ~isempty(colM)) && length(colM)==1
    % reconstruction is executed with leave-one-variable-out
    
    % variables that are not missing:
    colA        =	setdiff(1:N,colM)                   ;
    
    % set up imputation matrices: 
    C22         =	V(colA,:)*diag(lambda2)*V(colA,:)'	;
    C21         =	V(colA,:)*diag(lambda2)*V(colM,:)'	;
    %C11         =	V(colM,:)*diag(lambda2)*V(colM,:)'	;
    %C12         =	C21'                                ;
    
    % compute reconstruction
    Ymiss_hat	=	Y(:,colA)*(C22)^(-1)*C21            ;
    error2      =   (Ymiss_hat-Y(:,colM)).^2            ;
    
else
    T       =   ((Y-mu)./sigma)*V ;
    Yhat    =   (T*V'.*sigma+mu)  ;
    error2  =   sum((Yhat-Y).^2,2);
end