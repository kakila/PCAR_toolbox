function [mu,sigma]=fJackKnife(Z)
  
% Jack-knifing
nSample =	size(Z,1)                   ; % number of samples
mu      =	mean(Z,1)                   ; % compute mean criterion
Z       =	(mu*nSample-Z)/(nSample-1)	; % compute deviations of criterion from mean
sigma	=	std(Z,1)                    ; % standard deviation of deviations
