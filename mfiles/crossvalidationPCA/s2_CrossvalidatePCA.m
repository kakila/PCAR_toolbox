clc
clear all
close all
warning('off','stats:factoran:ZeroVariance')
warning('off','stats:factoran:ZeroVarianceSignificance')

%%
% ======================================
% LOAD DATA
% ======================================
%Ytil	=	dlmread('../../data/simdata.csv',';');
%Ytil	=	Ytil(:,1:end); 
%trueLV	=	dlmread('../../data/trueLV.csv',';');
Ytil	=	dlmread('../../data/Spectra_origin_all.csv',';',1,2);
%Ytil	=   dlmread('../../data/Spectra_rel_all.csv',';',1,2);
%Ytil	=	Ytil(:,40:71) ;
trueLV	=	nan                 ;

%%
% ======================================
% DEFINE MODEL STRUCTURE
% ======================================
model       =   'ppca'	; % applied model
centering	=	1           ; % apply mean centering (1) or not (0)
scaling     =	1           ; % apply unit variance scaling (1) or not (0)

%% 
% ======================================
% BUILD A 3-PC MODEL (PCA)
% ======================================
[mu,sigma,lambda2o,Vo]= fDecompose(Ytil,centering,scaling) ;

pc	=   3   ;
U	=   (((Ytil-mu)./sigma)*Vo(:,1:pc))./(lambda2o(1:pc)').^(1/2)   ;

% SHOW SCORE FOR 3RD PC
figure, 
for j=1:pc
    subplot(pc,1,j),
    stem(U(:,j),'.-')
end

%%
% ======================================
% DEFINE CROSS-VALIDATION METHOD
% ======================================

% block-wise cross-validation - define blocks:
% 1. Number of repetitions - assuming always the same number of repetitions
R	=   5   ;
% 2. dimensions of data set
[M,N]=size(Ytil) ;

% number of blocks, assuming all blocks have size 5x215
B           =   M/R                     ;
blockindex  =	repmat(1:B,[R 1])       ;
blockindex  =   blockindex(:)           ; 

% cross-validation pattern:
% for pca model: rkf, ekf, cekf, 
% for ppca model: rkf, ekf
method_cv           =	'rkf'   ;	

% estimation method:
%   for pca model and ekf/cekf: itr, pmp, tri, tsr
%   for pca model and rkf: msr
%   for ppca model and ekf: ign, crps, msr 
%   for ppca model and rkf: ign, ignres
method_estimation	=	'ign'	;	

%%
% ======================================
% ACTUAL CROSS-VALIDATION 
% ======================================
[CV_crit1,CV_crit1_sigma,CV_crit1_sigma2] = fCrossvalidation(Ytil,model,centering,scaling,method_cv,method_estimation,blockindex)  ;

%%
% ======================================
% VISUALIZATION 
% ======================================
PC= (1:length(CV_crit1))-1;
n =size(Ytil,2);
truePar = 1+sum(1:n)-sum(1:(n-trueLV));
for j=1:length(PC)
    nPar1(j) = 1+sum(1:n)-sum(1:(n-PC(j)));
end
CRIT = nan(length(PC),1);
CRIT(1:length(CV_crit1),1) =CV_crit1;

indexmin =find(CRIT(:,1)==min(CRIT(:,1)));

CRIT_sig = nan(length(PC),1);
CRIT_sig(1:length(CV_crit1),1) =CV_crit1_sigma2;
CRIT_sig(1:length(CV_crit1),1) =CV_crit1_sigma;

% figure, hold on
%     plot(nPar1(1:end),CRIT(:,1),'ko','MarkerFaceColor','k')
%     plot(nPar1(indexmin),CRIT(indexmin,1),'ko','MarkerFaceColor','r')
%     plot((nPar1(1:end)+[-1 ; +1]),(CRIT(:,1)+CRIT_sig(:,1)*[1 1])','k-')
%     Ylim=get(gca,'Ylim');
%     plot([1 1]*truePar,Ylim,'r-')
%     xlabel('#Parameters')
%     ylabel('Criterion')

inc = 1:(length(PC));

figure, hold on
    plot(PC(inc),CRIT(inc,1),'ko','MarkerFaceColor','k')
    plot(PC(indexmin),CRIT(indexmin,1),'ko','MarkerFaceColor','r')
    plot(PC(inc)+[-.2 ; +.2],(CRIT(inc,1)+CRIT_sig(inc,1)*[1 1])','k-')
    Ylim=get(gca,'Ylim');
    plot([1 1]*trueLV,Ylim,'r-')
    xlabel('#PCs')
    ylabel('Criterion')




