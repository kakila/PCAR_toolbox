clc
clear all
close all

warning('off','stats:factoran:ZeroVariance')
warning('off','stats:factoran:ZeroVarianceSignificance')


load('../../../Y31')
Ytil =Y3(1:50:end,1:end);
clear Y3;


model       =   'ppca'  ; % applied model
centering	=	1       ; % apply mean centering (1) or not (0)

% cross-validation pattern:
% for pca model: rkf, ekf, cekf, 
% for ppca model: rkf, ekf
method_cv           =	'rkf'   ;	

% estimation method:
%   for pca model and ekf/cekf: itr, pmp, tri, tsr
%   for pca model and rkf: msr
%   for ppca model and ekf: ign, crps, msr 
%   for ppca model and rkf: ign, ignres
method_estimation	=	'ign'	;	


[mu,lambda2,V,U,S]= fDecompose(Ytil,centering);

for bip=1:3
    
pc1=bip*3-2;
pc2=bip*3-1;
pc3=bip*3;
figure,
%subplot(2,2,1)
    scatter3(U(:,pc1),U(:,pc2),U(:,pc3),3,1:size(U,1))
    axis tight
% subplot(2,2,2)
%     scatter(include,U(:,pc2),3,include)
%     axis tight
% subplot(2,2,3)
%     scatter(U(:,pc1),include,3,include)
%     axis tight
    
    
drawnow()
end

[CV_crit,CV_crit_sigma,CV_crit_sigma2] = fCrossvalidation(Ytil,model,centering,method_cv,method_estimation)  ;

% Visualization
PCs = (1:length(CV_crit))-1;
figure,
    bar(PCs(1:end-1),CV_crit(1:end-1))
    %errorbar(PCs(1:end-1),CV_crit(1:end-1),CV_crit_sigma2(1:end-1))
    xlabel('#PCs')
    ylabel('Criterion')




