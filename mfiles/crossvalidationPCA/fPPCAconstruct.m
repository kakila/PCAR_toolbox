function [sigma2_hat,phi2_hat,lambda2_hat]=fPPCAconstruct(lambda2,A)

% construct PCA model

LVmax = length(lambda2) ;	% maximal number of PCs

% compute noise variance parameter:
if LVmax==A,	sigma2_hat	=	0                               ;
else,           sigma2_hat	=	sum(lambda2(A+1:end))/(LVmax-A) ;
end

% compute eigenvalues for denoised data:
phi2_hat            =   lambda2-sigma2_hat      ;
phi2_hat((A+1):end) =   0                       ;

% compute eigenvalues for noisy data:
lambda2_hat         =   (phi2_hat+sigma2_hat)	;