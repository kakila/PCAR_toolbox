function [error2,Yhat] = fPCAerror2impute(Y,mu,sigma,lambda2,V,nOV,colM,skillmethod,B) 

% compute squared error obtained via imputation

% centering:
Yc           =   (Y-mu)./sigma                ; 

switch skillmethod
    case 'itr' % iterative estimation
        
        % mean centering
        
        % initialization
        iternum     =   0                   ;
        M           =   size(Y,1)           ;
        relchange	=	inf(M,1)            ;
        Yest        =	zeros(M,1)          ;
        error2      =	(Yest-Yc(:,colM)).^2 ;
        
        % as long as there is no convergence yet
        while any(relchange>1e-3)
            
            % iteration update
            iternum         =   iternum+1                           ;
            
            % apply model with current estimate
            Ytag            =   Yc                                   ;
            Ytag(:,colM)    =   Yest                                ;
            Ttag            =	Ytag*V                              ;
            
            % reconstruct estimate
            Yest            =	Ttag*V(colM,:)'                     ;
            
            % compute new squared error and relative change
            error2_new      =	(Yest-Yc(:,colM)).^2                 ;
            relchange       =	abs(error2_new-error2)./abs(error2) ;
            error2          =	error2_new                          ;
            
        end
        
        % return complete input matrix with estimate
        Ytag(:,colM)    =   Yest	;
        Yhat            =	Ytag	;
        
    case 'pmp' % projection to model plane - numerically unstable. Use iterative estimation as equivalent method.
        
        % variables that are not missing
        colA	=	setdiff(1:nOV,colM) ;
        
        % initialize estimates
        Yhat	=	Yc                  ;
        
        % estimate missing variable
        if isempty(lambda2)
            Yhat(:,colM)	=	0       ;
        else
            Yhat(:,colM)	=	Yhat(:,colA)*V(colA,:)*(V(colA,:)'*V(colA,:))^(-1)*V(colM,:)'	;
        end
        
    case 'tri' % trimmed score imputation
        
        % initialize estimates
        Ytag            =   Yc      ;
        Ytag(:,colM)    =   0       ;
        
        % projection
        Ttag            =	Ytag*V          ;
        
        % reconstruction
        Yest            =	Ttag*V(colM,:)' ;
        
        % return complete input matrix with estimate
        Ytag(:,colM)    =   Yest    ;
        Yhat            =	Ytag	;
        
    case 'tsr' % trimmed score regression
        
        % initialize estimates
        Ytag            =   Yc      ;
        Ytag(:,colM)    =   0       ;
        
        % projection
        Ttag            =	Ytag*V	;
        
        % regression
        That            =	Ttag*B	;
        
        % reconstruction
        Yest            =	That*V(colM,:)'	;
        
        % return complete input matrix with estimate
        Ytag(:,colM)    =   Yest    ;
        Yhat            =	Ytag	;
        
    otherwise
        disp(['Imputation method (' skillmethod ') not supported.'])
end

% add mean vector
Yhat	=	Yhat.*sigma+mu              ;

% compute squared residual
error2	=	(Yhat(:,colM)-Y(:,colM)).^2	;