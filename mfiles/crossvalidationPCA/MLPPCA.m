function logpdf = MLPPCA(CovYtil,Yc,logsig,nFactor)

sig = 10.^logsig ;
Ycs = Yc./sig(:)';
[mu,lambda2o,V,U,S]= fDecompose(Ycs,0);
[sigma2,phi2,lambda2]=fPPCAconstruct(lambda2o,nFactor) ;
CovYhat = diag(sig)*(V*diag(lambda2)*V')*diag(sig) ; 

logpdf = trace(inv(CovYhat)*CovYtil) + log(det(CovYhat));


end

