
close all
inc=find(or(isnan(label),label==0)) ;
%inc = inc(inc>400000);
Y3 = Y2(inc,:) ;
Ytil3 = Ytil2(inc,:) ;
TT3=TT2(inc,:);

disp('Decomposition ...')
[mu,lambda2,V,U,S]= fDecompose(Y3(:,:),1);
disp('Decomposition done')

pc=1;
figure, hold on
    plot(inc(:),U(:,pc),'k.-')
    
    index = find(min(U(:,pc))==U(:,pc)) ;
    plot(inc(index+range),U(index+range,pc),'ro')
    
figure, hold on
    show = 1:1000:size(Y3,1);
    plot(waves1,Y3((show),:)','k-')
    plot(waves1,Y3((index+range),:),'r-')
    drawnow()
    
    