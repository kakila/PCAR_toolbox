function nll  = neglogfun(par,S)

n =size(S,1);
nFactor = (length(par)/n-1) ;
Psi = par(1:n) ;
L = reshape(par((n+1):end),[n nFactor]);
CovYhat = L*L'+diag(Psi) ;

nll =	trace(inv(CovYhat)*S) + log(det(CovYhat))   ;
    
    
end

