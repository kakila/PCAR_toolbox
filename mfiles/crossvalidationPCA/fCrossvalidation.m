function [skill_mu,skill_sigma,skill_mu_sigma,skill]=fCrossvalidation(Ytil,model,centering,scaling,cvmethod,skillmethod,blockindex)

% number of samples, number of observed variables
[nSample,nOV]	=	size(Ytil)          ;

% blockindex
if nargin<7 || isempty(blockindex)
    blockindex	=	(1:nSample)'        ;
else
    blockindex  =   blockindex(:)       ;
    assert(length(blockindex)==nSample,'Dimensions of "blockindex" do not match row size of "Ytil".')
end
blocks          =   unique(blockindex)  ;
nBlock          =   length(blocks)      ;

% compute maximum number of PCs, assuming Leave-One-Sample-Out scheme
Amax          = min(nSample-1,nOV-1)  ; % apply -1 since calibration data set has one sample less in LOOCV scheme.
if strcmp(model,'efa')
    df2 = @(a,n) ((a-nOV)^2 - (a+nOV));
    while df2(Amax,nSample-1)<0
        Amax = Amax-1;
    end
end
%Amax = min(11,Amax);

% select model structure:
switch model
    case 'pca'
        % selected structure: principal component analysis (PCA)
        disp('      Cross-validation PCA model')
        
        % ********************************************************************************
        % Prellocation
        % ********************************************************************************
        switch cvmethod % select cross-validation pattern
            % supported corss-validation patterns
            %   rkf: row-wise k-fold (implemented as as leave-one-out)
            %   ekf: element-wise k-fold (implemented as leave-one-out)
            %   cekf: corrected element-wise k-fold (implemented as leave-one-out)
            case 'rkf' 
                % only mean square residual criterion is supported:
                PermittedSkills =	{'msr'}                 ; 
                % pre-allocate matrix of results:
                skill           =	zeros(nSample,1,Amax+1) ; 
            case {'ekf','cekf'} 
                % supported criteria:
                %   itr: Iterative imputation
                %   pmp: Projection to model plane
                %   tri: Trimmed score imputation
                %   tsr: Trimmed score regression
                PermittedSkills = {'itr','pmp','tri','tsr'};
                skill   = zeros(nSample,nOV,Amax+1) ;
            otherwise
                disp(['         CV method (' cvmethod ') not supported for model type (' model ').'])
        end
        
        % making sure the selected criterion is available
        assert(ismember(skillmethod,PermittedSkills),['Skill (' skillmethod ') not supported for cross-validation type (' cvmethod ').'])
        
        % ********************************************************************************
        % Actual cross-validation loop
        % ********************************************************************************
        
        % FOR EVERY BLOCK:
        for iBlock=1:nBlock
            
            disp(['	Block ' num2str(iBlock) ' of ' num2str(nBlock)])
            
            % select the rows with validation data
            rowVal  =   find(blockindex==blocks(iBlock))    ;
            % select the rows with calibration data
            rowCal  =	setdiff(1:nSample,rowVal)           ;
            
            % =================================
            %   MATRIX DECOMPOSITION
            % =================================
            
            % compute mean and subtract it (if needed) and decompose with
            % SVD:
            [mu,sigma,lambda2o,Vo]= fDecompose(Ytil(rowCal,:),centering,scaling) ;
            
            % FOR EVERY NUMBER OF PRINCIPAL COMPONENTS:
            for A=0:Amax
                % =================================
                %   PCA MODEL SETUP
                % =================================
                
                % Compute/select PCA model elements,
                %   i.e. covariance matrix eigenvectors and eigenvalues 
                [V,lambda2]=fPCAconstruct(lambda2o,A,Vo) ;
                
                % =================================
                %   PPCA VALIDATION
                % =================================
                switch cvmethod
                    case 'rkf'
                        % compute the mean squared residual for the
                        % left-out sample
                        err2 = fPCAerror2(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV) ;  
                        skill(rowVal,1,A+1) = err2 ;
                    case 'ekf'
                        switch skillmethod
                            case 'tsr'
                                % FOR EVERY OBSERVED VARIABLE:
                                for iOV=1:nOV
                                    % -------------------------------------
                                    % Build regression model with
                                    % calibration data set
                                    % -------------------------------------
                                    Ytag = (Ytil(rowCal,:)-mu)./sigma ;
                                    if isempty(lambda2)
                                        B = [];
                                    else
                                        T = Ytag*V ;
                                        Ytag(:,iOV) = 0;
                                        Ttag = Ytag*V ;
                                        B = (Ttag'*Ttag)^(-1)*Ttag'*T ;
                                    end
                                    
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % regression method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate. 
                                    % -------------------------------------
                                    skill(rowVal,iOV,A+1) = fPCAerror2impute(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV,iOV,skillmethod,B) ;
                                    
                                end
                            case {'itr','pmp','tri'}
                                % FOR EVERY OBSERVED VARIABLE
                                for iOV=1:nOV
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % imputation method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate. 
                                    % -------------------------------------
                                    skill(rowVal,iOV,A+1) = fPCAerror2impute(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV,iOV,skillmethod) ;
                                end
                            otherwise
                                disp('         Unknown imputation method')
                        end
                    case 'cekf'
                        
                        % ------------------------------------------------
                        % Computed augmented matrix
                        % ------------------------------------------------
                        Y     =   (Ytil-mu)./sigma ;
                        T     =   Y*V ;
                        Yaug  = [ Y T ] ;
                        
                        % ------------------------------------------------
                        % Build augmented PCA model with calibration data
                        % ------------------------------------------------
                        [mu_aug,sigma_aug,lambda2o_aug,Vo_aug]= fDecompose(Yaug(rowCal,:),centering,0) ;
                        [V_aug,lambda2_aug]=fPCAconstruct(lambda2o_aug,A,Vo_aug) ;
                        
                        % ------------------------------------------------
                        % Apply augmented PCA model
                        % ------------------------------------------------
                        switch skillmethod
                            case 'tsr'
                                
                                for iOV=1:nOV
                                    % -------------------------------------
                                    % Build regression model with
                                    % calibration data set
                                    % -------------------------------------
                                    Ytag = Yaug(rowCal,:)-[mu zeros(1,A)] ;
                                    if length(lambda2)==0
                                        B = [];
                                    else
                                        T = Ytag*V_aug ;
                                        Ytag(:,iOV) = 0;
                                        Ttag = Ytag*V_aug ;
                                        B = (Ttag'*Ttag)^(-1)*Ttag'*T ;
                                    end
                                    
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % regression method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate. 
                                    % -------------------------------------
                                    skill(rowVal,iOV,A+1) = fPCAerror2impute(Yaug(rowVal,:),mu_aug,sigma_aug,lambda2,V_aug,nOV+A,iOV,skillmethod,B) ;
                                    
                                end
                            case {'itr','pmp','tri'}
                                for iOV=1:nOV
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % imputation method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate. 
                                    % -------------------------------------
                                    skill(rowVal,iOV,A+1) = fPCAerror2impute(Yaug(rowVal,:),mu_aug,sigma_aug,lambda2_aug,V_aug,nOV+A,iOV,skillmethod) ;
                                end
                            otherwise
                                disp('         Unknown imputation method')
                        end
                end
                
            end
        end
        
    case {'ppca','blockppca'}
        
        disp('      Cross-validation PPCA model')
        % ********************************************************************************
        % Prellocation
        % ********************************************************************************
        switch cvmethod
            case 'rkf'
                PermittedSkills =	{'ign','ignres'}            ;
                skill           =	inf(nSample,1,Amax+1,2)	;
            case 'ekf'
                PermittedSkills =	{'crps','ign','msr'}        ;
                skill           =	inf(nSample,nOV,Amax+1,3) ;
            otherwise
                disp(['         CV method (' cvmethod ') not supported for model type (' model ').'])
        end
        
        % making sure the selected criterion is available
        assert(ismember(skillmethod,PermittedSkills),['Skill (' skillmethod ') not supported for cross-validation type (' cvmethod ').'])
        
        % ********************************************************************************
        % Actual cross-validation loop
        % ********************************************************************************
        
        % FOR EVERY BLOCK:
        for iBlock=1:nBlock
            
            disp(['	Block ' num2str(iBlock) ' of ' num2str(nBlock)])
            
            % select the rows with validation data
            rowVal  =   find(blockindex==blocks(iBlock))    ;
            % select the rows with calibration data
            rowCal  =	setdiff(1:nSample,rowVal)           ;
            
            % =================================
            %   MATRIX DECOMPOSITION
            % =================================
            
            % compute mean and subtract it (if needed) and decompose with
            % SVD:
            switch model
                case 'ppca'
                    [mu,sigma,lambda2o,V]= fDecompose(Ytil(rowCal,:),centering,scaling) ;
                case 'blockppca'
                    [mu,sigma,lambda2o,V,U,S]= fDecompose(Ytil(rowCal,:),centering,scaling,'em',Amax,blockindex(rowCal));
                otherwise
            end
            
            for A=0:Amax
                % =================================
                %   PPCA MODEL SETUP
                % =================================
                
                % Compute additional PPCA model elements,
                %   i.e. noise variance (sigma2), eigenvalues of the
                %   denoised data (phi2) and eigenvalues of the noisy data
                %   (lambda2)
                [sigma2,phi2,lambda2]=fPPCAconstruct(lambda2o,A) ;
                
                % =================================
                %   PPCA VALIDATION
                % =================================
                switch cvmethod
                    case 'rkf'
                        switch skillmethod
                            case {'ign','ignres'}
                                % compute the ignorance score and the
                                % ignorance score in the residual space for
                                % the validation sample
                                switch model
                                    case 'blockppca'
                                        skill(rowVal,1,A+1,:) = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV,[],A,1) ;
                                    case 'ppca'
                                        skill(rowVal,1,A+1,:) = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV,[],A) ;
                                    otherwise
                                end
                            otherwise
                                disp(['         Skill (' skillmethod ') not supported for cross-validation type (' cvmethod ').'])
                        end
                        
                    case 'ekf'
                        for iOV=1:nOV
                            % compute the ignorance score, the squared
                            % error, and the continuous ranked probability
                            % score (crps) for the validation sample
                            [ign_val,error2,cprs_val]  = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV,iOV) ;
                            skill(rowVal,iOV,A+1,:) = [ign_val,error2,cprs_val] ;
                        end
                        
                end
            end
        end
        
        % select the subset of the computed matrix "skill" to obtain the
        % criterion that is wanted.
        switch skillmethod
            case 'ign'
                skill = (skill(:,:,:,1)) ;
            case {'ignres','msr'}
                skill = (skill(:,:,:,2)) ;
            case {'crps'}
                skill = (skill(:,:,:,3)) ;
            otherwise
                disp(['         Skill (' skillmethod ') not supported for cross-validation type (' cvmethod ').'])
        end
    case 'efa' % exploratory factor analysis
        disp('      Cross-validation EFA model')
        % ********************************************************************************
        % Prellocation
        % ********************************************************************************
        switch cvmethod
            case 'rkf'
                PermittedSkills =	{'ign','ignres'}            ;
                skill           =	zeros(nSample,1,Amax+1,2)	;
            case 'ekf'
                PermittedSkills =	{'crps','ign','msr'}        ;
                skill           =	zeros(nSample,nOV,Amax+1,3) ;
            otherwise
                disp(['         CV method (' cvmethod ') not supported for model type (' model ').'])
        end
        
        % making sure the selected criterion is available
        assert(ismember(skillmethod,PermittedSkills),['Skill (' skillmethod ') not supported for cross-validation type (' cvmethod ').'])
        
        % ********************************************************************************
        % Actual cross-validation loop
        % ********************************************************************************
        
        % FOR EVERY BLOCK:
        for iBlock=1:nBlock
            
            disp(['	Block ' num2str(iBlock) ' of ' num2str(nBlock)])
            
            % select the rows with validation data
            rowVal  =   find(blockindex==blocks(iBlock))    ;
            % select the rows with calibration data
            rowCal  =	setdiff(1:nSample,rowVal)           ;
            
            if centering
                mu  = mean(Ytil,1)   ;
            else
                mu  = zeros(1,nOV)  ;
            end
            Yc     = Ytil-mu        ;
            if scaling
                sigma = std(Yc,[],1);
            else
                sigma = ones(1,nOV) ;
            end
            Ycs = Yc./sigma ;

            CovY = cov(Ycs);
            
            for A=0:Amax
                
                % =================================
                %   MATRIX DECOMPOSITION
                % =================================
                
                % compute mean and subtract it (if needed) and decompose with
                % Factor Analysis, report result in PCA format:
                [mu1,sigma1,lambda2o,V]= fDecompose(Ycs(rowCal,:),0,0,'factoran',A) ;
                
                % =================================
                %   EFA MODEL SETUP
                % =================================
                
                %svd(V'*V+diag(la)
                % Compute additional PPCA model elements,
                %   i.e. noise variance (sigma2), eigenvalues of the
                %   denoised data (phi2) and eigenvalues of the noisy data
                %   (lambda2)
                [sigma2,phi2,lambda2]=fPPCAconstruct(lambda2o,A) ;
                
                % =================================
                %   PPCA VALIDATION
                % =================================
                switch cvmethod
                    case 'rkf'
                        switch skillmethod
                            case {'ign','ignres'}
                                % compute the ignorance score and the
                                % ignorance score in the residual space for
                                % the validation sample
                                skill(rowVal,1,A+1,:) = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV,[],A) ;
                            otherwise
                                disp(['         Skill (' skillmethod ') not supported for cross-validation type (' cvmethod ').'])
                        end
                        
                    case 'ekf'
                        for iOV=1:nOV
                            % compute the ignorance score, the squared
                            % error, and the continuous ranked probability
                            % score (crps) for the validation sample
                            [ign_val,error2,cprs_val]  = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,nOV,iOV) ;
                            skill(rowVal,iOV,A+1,:) = [ign_val,error2,cprs_val] ;
                        end
                        
                end
            end
        end
        
        % select the subset of the computed matrix "skill" to obtain the
        % criterion that is wanted.
        switch skillmethod
            case 'ign'
                skill = (skill(:,:,:,1)) ;
            case {'ignres','msr'}
                skill = (skill(:,:,:,2)) ;
            case {'crps'}
                skill = (skill(:,:,:,3)) ;
            otherwise
                disp(['         Skill (' skillmethod ') not supported for cross-validation type (' cvmethod ').'])
        end
        
    otherwise
        disp('         unknown model type')
        
end

% ********************************************************************************
% Averaging and jack-knifing
% ********************************************************************************
%disp('Averaging and Jack-knifing')
dims                      =   size(skill)                                       ;
Z                         =   reshape(skill,[ prod(dims(1:2)) dims(3:end) ])	;
[skill_mu,skill_mu_sigma] =   fJackKnife(Z)                                     ;
skill_sigma               =   std(Z,[],1)                                       ;
