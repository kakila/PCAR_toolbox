function [ign_val,error2,crps_val]  = fPPCAvalidate(Yval,mu_hat,sigma_hat,lambda2_hat,V,N,colM,A,block)

if nargin<9 || isempty(block)
    block=false;
end
% function used to validate PPCA model

% compute standard deviations from component variances
lambda_hat	=	lambda2_hat.^(1/2)	;

if (nargin >=6 && ~isempty(colM)) && length(colM)==1
    % IF: validating based on imputation
    
    Yvalms	=   (Yval-mu_hat)./sigma_hat                ;
    
    % variables that are not missing
    colA	=	setdiff(1:N,colM)                       ;
    
    % compute submatrices of covariance matrix
    C22     =	V(colA,:)*diag(lambda2_hat)*V(colA,:)'	;
    C11     =	V(colM,:)*diag(lambda2_hat)*V(colM,:)'	;
    C21     =	V(colA,:)*diag(lambda2_hat)*V(colM,:)'	;
    C12     =	C21'                                    ;
    
    % compute mean estimate
    Ymiss_hat       =	Yvalms(:,colA)*(C22)^(-1)*C21	;
    % variance of estimate
    sigma2_miss_hat	=	C11-C12*(C22)^(-1)*C21          ;
    % standard deviation of estimate
    sigma_miss_hat	=	sigma2_miss_hat.^(1/2)          ;
    
    % compute Z-score of residual
    Z               =	(Ymiss_hat-Yvalms(:,colM))/sigma_miss_hat	;
    if block
        Z           =   repmat(mean(Z,1),[size(Z,1) 1])   ;
    end
    
    % compute log-likelihood
    ign_val = log(2*pi)*1/2+ sum( log(sigma_miss_hat))+ (   ( Z .^ 2 / 2) )   ;
    
    % compute squared residual (in original scale)
    if nargout>=2
        error2 = ( (Ymiss_hat.*sigma_hat(colM)+mu_hat(colM) )-Yval(:,colM) ).^2 ;
    end
    
    % compute continuous ranked probability score (CRPS)
    if nargout >=3
        phi         =	(2 * pi)^(- 1/2) * exp (- Z .^ 2 / 2)               ;
        PHI         =	erfc (Z / (-sqrt(2))) / 2                           ; 
        crps_val	=	-sigma_miss_hat*(1/(sqrt(2*pi))-2*phi-Z.*(2*PHI-1)) ;
    end
else
    % ELSE: validating whole sample at once
    Yms = ((Yval-mu_hat)./sigma_hat) ;
    if block
        R = size(Yms,1) ;
        Yms = Yms'; % RxN
        Yms = Yms(:); % R.N x 1
        Vb = repmat(V,[R 1]); % R.NxC
        T = pinv(Vb)*Yms ;
        T = repmat(T(:)',[ R 1 ]) ;
    else
        T       =   Yms*V ;
    end
    if size(T,2)==0
        X=T;
    else
        X       =   T./lambda_hat'  ;
    end
    
    ign_val =   +log(2*pi)*N/2+ sum( log(lambda_hat))+sum( (   ( X .^ 2 / 2) ) ,2)  ;
    
    if (nargin >=7 && ~isempty(A)) && length(A)==1
        X = X(:,A+1:end) ;
        ign_val(:,2) =   +log(2*pi)*(N-A)/2+ sum( log( lambda_hat(A+1:end)  ))+sum( (   ( X .^ 2 / 2) ) ,2)  ;
    end
    
end