function [mu,sigma,lambda2,V,U,S]= fDecompose(Y,center,scaling,method,nFactor,blockindex)

if nargin<4 || isempty(method)
    method ='svd';
end

% number of sample, number of observed variables
[M,N] = size(Y) ;

% blockindex
if nargin<6 || isempty(blockindex)
    blocks	=	[];
else
    blockindex  =   blockindex(:)       ;
    [blockindex,index]=sort(blockindex);
    Y =Y(index,:) ;
    assert(length(blockindex)==M,'Dimensions of "blockindex" do not match row size of "Ytil".')
    blocks          =   unique(blockindex)  ;
    
end
nBlock          =   length(blocks)      ;

% ==============================
% CENTERING
% ==============================
if center
    mu  = mean(Y,1)   ;
else
    mu  = zeros(1,N)  ;
end
Y    = Y-mu        ;
if scaling
    sigma = std(Y,[],1);
else
    sigma = ones(1,N) ;
end
Y = Y./sigma ;
% ==============================
% ==============================

switch method
    case 'svd'
        % ==============================
        % SVD DECOMPOSITION
        % ==============================
        [U,S,V] = svd(Y,0)            ;
        lambda2 = diag(S).^2/M        ; % assumes input matrix is data, not a covariance matrix (!)
        lambda2 = lambda2(:)          ;
        % ==============================
        % ==============================
    case 'em'
        
        % Approach assumes input matrix is data, not a covariance matrix (!)
        
        % Initialize model
        [U,S,V] = svd(Y,0)            ;
        T   =   U*S     ;
        T   =   T(:,1:nFactor)       ;
        P   =   V(:,1:nFactor)       ;
        
        if nBlock==0
        else
            J = inf;
            iter= 0;
            itermax =1000;
            mu1=zeros(N,1);
            if nFactor==0
                if center
                    T1 = [ ones(M,1) ];
                    mP = Y'*T1/(T1'*T1);
                    mu1 = mP(:,1);
                else
                    mu1= zeros(N,1);
                end
                Yhat    =  repmat(mu1(:)',[M 1]); % estimated data in scaled form
            else
                while iter<itermax
                    iter=iter+1;
                    % update scores
                    for iBlock=1:nBlock
                        rows = find(blockindex==blocks(iBlock));
                        Yblock = Y(rows,:)';
                        if center
                            Yblock =Yblock-mu1 ;
                        end
                        Yblock = Yblock(:)' ;
                        R = length(rows) ;
                        Pblock = repmat(P,[ R 1 ]) ;
                        % obtain new scores via regression:
                        %   minimize_{T} sum( (Yblock - Tblock*Pblock').^2 )
                        %   N*R         N*R x C     x	CxC
                        Tblock = Yblock	*   Pblock      / (Pblock'*Pblock) ;
                        %Tblock = Yblock	*   Pblock      *   eye(nFactor)*1/R ;
                        T(rows,:)  = repmat(Tblock,[ R 1]) ;
                    end
                    
                    % update components
                    %   minimize_{P} sum( (Yblock - (1 * mu'+T*P')).^2 )
                    %   N*R         N*R x C     x	CxC
                    %(Y'*T*inv(T'*T)) = P
                    if center
                        T1 = [ ones(M,1) T ];
                        mP = Y'*T1/(T1'*T1);
                        mu1 = mP(:,1);
                        P = mP(:,2:end);
                    else
                        P = Y'*T/(T'*T);
                        mu1= zeros(N,1);
                    end
                    v = sum(P.^2,1);
                    P=P./(v.^(1/2));
                    
                    % evaluate model
                    Jnew = norm(Y-T*P');
                    if ((J-Jnew)/Jnew)<(1e-9)
                        iter = inf;
                    end
                    J=Jnew;
                end
                Yhat    =   (T*P'+mu1(:)'); % estimated data in scaled form
            end
            
        end
        
        if center
            % new mean
            mu1 = mean(Yhat);
            % modify known mean in original scale:
            mu = mu+mu1.*sigma ;
            % re-center modelled data
            Yhat=Yhat-mu1(:)' ;
        end
        [U,S,V] = svd(Yhat,0)            ;
        lambda2 = diag(S).^2/M        ; % assumes input matrix is data, not a covariance matrix (!)
        lambda2 = lambda2(:)          ;
        
    case 'factoran'
        optimset = statset('factoran')   ;
        optimset.MaxFunEvals = inf;
        optimset.MaxIter = inf;
        stdev = std(Y,1);
        Ys = Y./stdev(:)';
        corrY= (Ys'*Ys)/M;
        [V,lambda2,R]=factoran(corrY,nFactor,'Xtype','covariance','Rotate','none','start','Rsquared','optimopts',optimset);
        % Construct estimated covariance matrix according to obtained model:
        CovY = diag(stdev)*(V*V'+diag(lambda2))*diag(stdev);
        % Decompose estimated covariance matrix to obtain in standard PCA
        % form:
        [U,S,V] = svd(CovY,0)            ;
        lambda2= diag(S);
        if nargout>=4
            U=Y*V*diag(1./lambda2.^(1/2));
        end
end