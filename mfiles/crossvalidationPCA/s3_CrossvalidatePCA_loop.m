clc
clear all
close all

Ytil = dlmread('../../data/simdata.csv',';');

for centering=0:1
    models = {'pca','ppca'};
    % models = {'ppca'};
    for iModel =1:length(models)
        model = models{iModel};
        switch model
            case 'pca'
                methods_cv = {'rkf','ekf','cekf'}   ;
            case 'ppca'
                methods_cv = {'rkf','ekf'};
        end
        % methods_cv = {'rkf'} ;
        
        for iMethod=1:length(methods_cv)
            method_cv = methods_cv{iMethod};
            switch method_cv
                case 'rkf'
                    switch model
                        case 'pca'
                            methods_estimation  = {'msr'};
                        case 'ppca'
                            methods_estimation  = {'ign','ignres'};
                    end
                    % methods_estimation  = {'ign'};
                case {'ekf','cekf'}
                    switch model
                        case 'pca'
                            methods_estimation  = {'itr','pmp','tri','tsr'};
                        case 'ppca'
                            methods_estimation  = {'crps','ign','msr'};
                    end
            end
            
            for iEstimation=1:length(methods_estimation)
                method_estimation = methods_estimation{iEstimation};
                
                disp([num2str(centering) ' ' model ' ' method_cv ' ' method_estimation ] )
                [CV_crit] = fCrossvalidation(Ytil,model,centering,method_cv,method_estimation)  ;
                
                % Visualization
                PCs = (1:length(CV_crit))-1;
                figure,
                bar(PCs(1:end-1),CV_crit(1:end-1))
                xlabel('#PCs')
                ylabel('Criterion')
                
                
            end
        end
    end
end

return


model = 'ppca'; % applied model
centering = 0 ; % apply mean centering (1) or not (0)

% cross-validation pattern:
% for pca model: rkf, ekf, cekf, 
% for ppca model: rkf, ekf
method_cv           =	'rkf'   ;	% any from: 

% estimation method:
%   for pca model and ekf/cekf: itr, pmp, tri, tsr
%   for pca model and rkf: msr
%   for ppca model and ekf: ign, crps, msr 
%   for ppca model and rkf: ign, ignres
method_estimation	=	'ign'	;	
centering           =	0       ;   % apply mean centering (1) or not (0)  

[CV_crit] = fCrossvalidation(Ytil,model,centering,method_cv,method_estimation)  ;

% Visualization
PCs = (1:length(CV_crit))-1;
figure,
    bar(PCs(1:end-1),CV_crit(1:end-1))
    xlabel('#PCs')
    ylabel('Criterion')




