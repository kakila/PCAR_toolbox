% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  

% Author: Kris Villez <Kris.Villez@eawag.ch>
% Last version: 2017.03.10

function hndl_bissect = addbissectline
% Adds a bissectrice (bissecting line) to the current axes
%
%  The data matrix is centered before computing the PCA by removing the
%  column mean of the data.
%
%   [hndl] = ADDBISECTLINE ()
%     Adds abissecting line and returns the handle (hndl) of this element.
%
%   Example:
%
%     x = rand (42, 1);
%     y = rand (42, 1);
%     plot ( x(:), y(:), 'o');
%     hold on
%     hndl_biss = addbissectline ();
%     set(hndl_biss,'Color','r')
%     axis tight
%     hold off
%

  limX  = get (gca, 'Xlim') ;
  limY  = get (gca, 'Ylim') ;
  minXY = min (limX(1), limY(1)) ;
  maxXY = max (limX(2), limY(2)) ;
  limXY = [ minXY maxXY ];

  set (gca, 'Xlim', limXY, 'Ylim', limXY)

  hndl_bissect=plot (limXY, limXY, 'k--');

end

%!demo
%! x = rand (42, 1);
%! y = rand (42, 1);
%! plot ( x(:), y(:), 'o');
%! hold on
%! hndl_biss = addbissectline ();
%! set(hndl_biss,'Color','r');
%! axis tight
%! hold off
