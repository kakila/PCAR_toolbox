% Copyright 2017-2018 Kris Villez
%
% This file is part of the principal component analysis and regression
% (PCAR) toolbox for Matlab/Octave. 
% 
% The PCAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The PCAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the PCAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  

% Author: Kris Villez <Kris.Villez@eawag.ch>
% Latest version: 2017.04.25

function h = addarrows(mm, P, colour)
% Adds an arrow to the current axes
%
%  H = ADDARROWS (MM, P, COLOUR)%
%  The arrow starting point is MM (d-by-1 array) and the tip is P (d-by-n array).
% Several arrows (n > 1) will be plotting with the same starting point.
% 
%  COLOUR is a color option passed to the corresponding quiver function 
%  (either 2d or 3d).
%
%  Example:
%
%    % Plot 3 arrows
%    p0 = [0; 0];
%    p1 = [0 1 1; 1 0 1];
%    h = addarrows (p0, p1, 'k');
%    axis equal
%
% See also quiver, quiver3
  [n m] = size (P);
  switch n
      case 2
        quiver_func =@(pp) quiver (mm(1), mm(2), pp(1), pp(2), 'Color', colour);
      case 3
        quiver_func =@(pp) quiver3 (mm(1), mm(2), mm(3), pp(1), pp(2), pp(3), ...
                'Color', colour);
      otherwise
          error ('dimensions do not allow for arrow display\n')
  end


  ax = gca;
  nohold = ~ishold();
  if nohold
    hold on;
  end
  
  htmp = zeros (m, 1);
  for j=1:m
      pp = P(:, j);
      htmp(j) = quiver_func (pp);
  end
  
  if nohold
    hold off
  end
  
  if nargout > 0
    h = htmp;
  end
end

%!demo
%! p0 = [0; 0];
%! p1 = [1 0 1; 0 1 1];
%! h = addarrows(p0, p1, 'k');
%! set(h(1), 'color', 'r')
%! set(h(2), 'color', 'b')
%! set(h(3), 'color', 'g')
%! axis equal

