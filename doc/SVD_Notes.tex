\documentclass[10pt,english,final]{article}

\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hyperref} % Hyperlinks within and outside the document
\hypersetup{
unicode=false,          % non-Latin characters in Acrobat’s bookmarks
pdfauthor={JuanPi Carbajal},%
pdftitle={},%
colorlinks=true,       % false: boxed links; true: colored links
linkcolor=OliveGreen,          % color of internal links
citecolor=Sepia,        % color of links to bibliography
filecolor=magenta,      % color of file links
urlcolor=NavyBlue,          % color of external links
}

\usepackage[colon]{natbib}
\usepackage{nicefrac}

\usepackage[utf8]{inputenc}
\usepackage{textcomp}
\usepackage{lmodern} % German related symbols
\usepackage{fouriernc} % Use the New Century Schoolbook font, comment this line to use the default LaTeX font or replace it with another
%\usepackage{roboto}
\usepackage[T1]{fontenc}


\usepackage{babel}
\usepackage{graphicx}
\usepackage[small]{caption}
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage{nicefrac}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{bm}

\usepackage{tikz}
\usetikzlibrary{arrows.meta,bending,matrix,positioning}

\usepackage{pgf,calc}
\usepackage{pgfplots}
\pgfplotsset{every axis/.append style={line width=1pt}}
\pgfplotsset{compat=newest}

% CC license
\usepackage[
    type={CC},
    modifier={by-sa},
    version={4.0},
]{doclicense}

% Custom Commands
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\stimes}{{\!\times\!}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\trp}{\top}
\newcommand{\preup}[2]{{}^{#2}\!{#1}}
% Data as feature and signal
\newcommand{\feature}[1]{\vec{#1}}
%\newcommand{\tensor}[1]{\bm{#1}}
\newcommand{\signal}[1]{\mathcal{#1}}

% Dimensions
\newcommand{\SpacDim}{n}
\newcommand{\SampDim}{t}

% Operators
\DeclareMathOperator*{\err}{err}
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\armax}{arg\,max}
\DeclareMathOperator*{\linspan}{span}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}
\DeclareMathOperator*{\sign}{sign}
\DeclareMathOperator*{\identity}{id}

% Comments
\newcommand{\jpi}[1]{{\color{Magenta} JPi: #1}}
\pagestyle{headings}

\graphicspath{{img}}

\begin{document}

\title{Singular Value Decomposition formulation of Principal Components Analysis}
\author{Juan Pablo Carbajal\thanks{Eawag {\tt\small  juanpablo.carbajl@eawag.ch}}}
\date{\today\\\vspace{3em}{\small\doclicenseThis}}

\maketitle


%\begin{abstract}

%\end{abstract}
\section{Conventions}
%\begin{equation}
%\feature{x} \equiv \begin{pmatrix} x_1, & \dots &, x_\SpacDim \end{pmatrix} \in \mathbb{R}^{\SpacDim}
%\end{equation}

%\begin{equation}
%  \feature{X} \equiv \begin{bmatrix}\feature{x}_1 \\ \vdots \\ \feature{x}_{\SampDim}\end{bmatrix} = 
%  \begin{bmatrix}\bm{x}_1^\trp \\ \vdots \\ \bm{x}_{\SampDim}^\trp \end{bmatrix} = 
%  \begin{bmatrix}\bm{x}_1 & \dots & \bm{x}_\SampDim \end{bmatrix}^\trp \equiv X^\trp
%\end{equation}

\begin{equation}
  \bm{x} \equiv \begin{bmatrix} x_1 \\ \vdots \\ x_r \end{bmatrix} \in \mathbb{R}^{r \stimes 1} (\equiv \mathbb{R}^r)
\end{equation}

The name of a matrix without decoration indicates the names used for its columns, for example $X \in \mathbb{R}^{r \stimes c}$ means

\begin{equation}
  X \equiv \begin{bmatrix} \bm{x}_1 & \dots & \bm{x}_c \end{bmatrix}
\end{equation}

We will use the decoration $\feature{X}$ when we want to understand the matrix as built from the rows of $X$, i.e. $\feature{X} = X^\trp$

\begin{equation}
{\color{OliveGreen}\feature{X}^\trp \equiv}
  \begin{tikzpicture}[%
        node distance=1mm and 0mm,%
        baseline]

    \matrix (M) [matrix of math nodes,%
             {left delimiter  = [},%
             {right delimiter = ]}]
    {
      x_{11} & \dots & x_{1c} \\
      \vdots & \ddots & \vdots \\
      x_{r1} & \dots & x_{rc} \\
    };
    % Column vector
    \draw[BrickRed,very thick] 
            (M-1-1.north west) -| (M-3-1.south east) -| (M-1-1.north west);
    \node (fv) [below left=of M.south west,align=right] {$\bm{x}_1$};
    \draw[BrickRed!60,very thick,shorten >=1mm,-{Stealth[bend]}] 
            (fv.east) to [out=0,in=270] (M-3-1.south);
    % Row vector
    \draw[OliveGreen,very thick] 
            ([xshift=-2pt, yshift=2pt]M-1-1.north west) -| 
            ([xshift=2pt, yshift=-2pt]M-1-3.south east) -| 
            ([xshift=-2pt, yshift=2pt]M-1-1.north west);
    \node (fv) [above left=of M.north west,align=right] {$\feature{x}_1^\trp$};
    \draw[OliveGreen,very thick,shorten >=1mm,-{Stealth[bend]}] 
            (fv.east) to [out=0,in=-270] (M-1-1.north);

  \end{tikzpicture}
  {\color{BrickRed} \equiv X}
\end{equation}

\noindent Note that $\feature{x}_i^\trp \neq \bm{x}_i$, since the two vectors only share one element in common, namely $\left(\feature{x}_i\right)_i = \left(\bm{x}_i\right)_i \equiv X_{ii}$.

The vector $\bm{x}_i$ is sometimes written as $X_{:i}$ to make explicit its location on the matrix, similarly $\feature{X}_{:i} \equiv \feature{x_i}$.
By definition, we have that $X_{i:} = \left(X^\trp\right)_{:i} = \feature{X}_{:i}\equiv \feature{x}_i$.

The two vectors, $\bm{x}_i$ and $\feature{x}_i$, represent two views of the matrix.
In the context of Gaussian processes, these views are called Weight-space View
($\feature{x}$) and the Function-space View ($\bm{x}_i$).
In machine learning the vector $\feature{x}$ is usually a feature vector, while
$\bm{x}$ is sometimes called variable, signal (e.g. the signal of a sensor), or function.

The matrix 
\begin{equation}
\Sigma(Y) = \frac{1}{r - 1} Y Y^\trp \label{eq:covM}
\end{equation}

\noindent is called the covariance matrix (of the column vectors $\bm{y}$) if the 
row mean of $Y$ have been subtracted.

The matrix
\begin{equation}
G(Y) = Y^\trp Y \label{eq:covM}
\end{equation}

\noindent is called the Gram or scalar product matrix (of the column vectors $\bm{y}$).
The following relation holds for matrices with zero row mean
\begin{equation}
\Sigma(Y) \propto \left(Y^\trp\right)^\trp Y^\trp = G\left(Y^\trp\right)
\end{equation}

\section{Introduction}
\subsection{Basis in finite dimensional vector spaces}
A basis of $\mathbb{R}^n$ is a set $\bset{\bm{e}_i}$ of $n$ linearly independent vectors.
Any vector $\vec{v} \in \mathbb{R}^n$ can be written as a linear combination $\bm{v} = \sum_{i=1}^n c_i \bm{e}_i$.
We can build the basis matrix $E = \begin{bmatrix} \bm{e}_1 & \ldots & \bm{e}_n\end{bmatrix}$.
The scalars $\bset{c_i}$ are the components of the vector $\bm{v}$ in the basis $E$.
Then any vector can be written as 
\begin{equation}
\bm{v}^\top = \bm{c}^\top E^\top = \begin{bmatrix} c_1 & \ldots & c_n \end{bmatrix} \begin{bmatrix} \bm{e}_1^\top \\ \vdots \\ \bm{e}_n^\top\end{bmatrix} = c_1 \bm{e}_1^\top + \ldots + c_n \bm{e}_n^\top
\end{equation}
where $\bm{c}^\top = \begin{bmatrix} c_1 & \ldots & c_n \end{bmatrix}$ are the components of the vector $\bm{v}$ in the basis $E$. 
It is how $\bm{v}$ looks like in the basis $E$.
Different basis give different components for the same vector.

An orthonormal basis is one such that the basis vectors are orthogonal and the norm of each vector is unit, i.e. $\bm{e}_i \cdot \bm{e}_j = \delta_{ij}$.
In terms of the basis matrix
\begin{equation}
E^\top E = \begin{bmatrix} \bm{e}_1^\top \\ \vdots \\ \bm{e}_n^\top\end{bmatrix} \begin{bmatrix} \bm{e}_1 & \ldots & \bm{e}_n\end{bmatrix} = I_{n \stimes n}
\end{equation}
This also means that $E^{-1} = E^\top$ (orthonormal matrix).

A matrix $A$ can be diagonalized if we can find a basis such that $A = E D E^{-1}$.

\section{Data covariance matrix and PCA}
$\cov(x,y) = \mathbb{E}\left[(x - \bar{x})(y - \bar{y})\right]$

Take $X \in \mathbb{R}^{\SampDim \stimes \SpacDim}$, $\tilde{X} = X - \bar{X}$

\begin{equation}
\Sigma(X) = \frac{1}{\SampDim - 1} \left(X - \bar{X}\right)^\top \left(X - \bar{X}\right) \label{eq:covM}
\end{equation}

PCA diagonalizes this matrix 
\begin{equation}
\Sigma(X) = E \Lambda E^{-1} \label{eq:PCA}
\end{equation}

\section{Singular value decomposition}

\jpi{TODO: explain matrices properties and shapes, i.e. reduced SVD}

Take $X \in \mathbb{R}^{\SampDim \stimes \SpacDim}$
\begin{equation}
  X = U S V^\trp \label{eq:svd}
\end{equation}

\noindent $U \in \mathbb{R}^{t \stimes n}$, $S, V \in \mathbb{R}^{n \stimes n}$, and

\begin{align}
U^\trp U = I_{n \stimes n} \; &\Longleftrightarrow \; \bm{u}_i^\trp \bm{u}_j = \delta_{ij} \label{eq:Uortho}\\
V^\trp V = I_{n \stimes n} \; &\Longleftrightarrow \; \bm{v}_i^\trp \bm{v}_j = \delta_{ij}\\
S \text{ is diagonal} \; &\Longleftrightarrow \; S_{ij} = \delta_{ij} \bm{s}_i
\end{align}

If the columns means have been removed from $X$ we get that the covariance matrix
is written as

\begin{equation}
\Sigma(X) = \frac{1}{\SampDim - 1} X^\trp X = (V S U^\trp)(U S V^\trp) =  V \frac{S^2}{\SampDim - 1} V^\trp \in \mathbb{R}^{\SpacDim \stimes \SpacDim} \label{eq:feacovM}
\end{equation}

comparing with eq.~\eqref{eq:PCA} we see that 
\begin{equation}
E = V \quad \Lambda = \frac{S^2}{\SampDim - 1}
\end{equation}

\section{POD}

We can associate the products in the factorization~\eqref{eq:svd} in two ways

\begin{align}
  X &= U \left(S V^\trp\right) \equiv U L \label{eq:basis}\\
  X &= \left(U S\right) V^\trp \equiv P^\trp V^\trp \label{eq:feabasis}
\end{align}

\noindent Eq.~\eqref{eq:basis} says that each column in $X$ is a linear combination
of the columns in $U$, that is

\begin{equation}
  X_{:i} \equiv \bm{x}_i = U L_{:i} = \begin{bmatrix}\bm{u}_1 & \dots & \bm{u}_\SpacDim \end{bmatrix}\begin{bmatrix}l_{1i} \\ \vdots \\ l_{\SpacDim i} \end{bmatrix} = \sum_{k=1}^\SpacDim \bm{u}_k l_{ki}
\end{equation}

\noindent In other words, each $\bm{x}_i$ is a linear combination of the $\SampDim$-dimensional $\bset{\bm{u}_i}$ vectors.
They are an orthonormal (due to eq.~\eqref{eq:Uortho}) basis of a $n$-dimensional subspace
of $\mathbb{R}^\SampDim$.
If $\SampDim > \SpacDim$ (the usual setup) this is a proper subspace.

Eq.~\eqref{eq:feabasis} can be interpreted in the same way if we take the transpose of $X$,

\begin{align}
  X^\trp  = \feature{X} &= V P \\
  \feature{X}_{:i} \equiv \feature{x}_i &= V P_{:i} = \begin{bmatrix}\bm{v}_1 & \dots & \bm{v}_\SpacDim \end{bmatrix}\begin{bmatrix}p_{1i} \\ \vdots \\ p_{\SpacDim i} \end{bmatrix} = \sum_{k=1}^\SpacDim \bm{v}_k p_{ki}
\end{align}

\noindent As before, each $\feature{x}_i$ is a linear combination of the $\SpacDim$-dimensional $\bset{\bm{v}_i}$ vectors.
They are an orthonormal basis of the whole $\mathbb{R}^\SpacDim$ space.

All this is equivalent to considering the decomposition of $X = USV^\trp$ and $\feature{X} = \feature{U} \feature{S} \feature{V}^\trp$ separately, and finding the connection between the matrices, namely 

\begin{align}
  \feature{X} = \feature{U} \feature{S} \feature{V}^\trp &\equiv X^\trp = (U S V^\trp)^\trp = V S U^\trp\\
  \feature{U} &\equiv V\\
  \feature{V} &\equiv U\\
  \feature{S} &\equiv S
\end{align}

\noindent since the decomposition is unique.
Therefore the interpretation of eq.~\eqref{eq:feabasis} follows from the one of eq.~\eqref{eq:basis} by replacement.

The covariance matrix of the features (given by~\eqref{eq:covM}) is

\begin{align}
\Sigma &= \frac{1}{\SampDim - 1}\feature{X} \feature{X}^\trp = \frac{1}{\SampDim - 1} X^\trp X = \frac{1}{\SampDim - 1} V S^2 V^\trp \in \mathbb{R}^{\SpacDim \stimes \SpacDim} \label{eq:feacovM}\\
\Sigma &= \frac{1}{\SampDim - 1}\sum_{i=1}^{\SampDim} \feature{x}_i \feature{x}_i^\trp \label{eq:feacov}\\
\Sigma_{ij} &= \frac{1}{\SampDim - 1} \bm{x}_i^\trp \bm{x}_j \label{eq:gramsig}
\end{align}

\noindent when the mean of the columns of $X$ was removed.
In general (i.e. with or without mean or normalization) it is also the Gram or scalar product matrix of the signals (due to eq.~\eqref{eq:gramsig}), i.e. a matrix filled with the scalar products of the signals.

Similarly, the matrix 
\begin{align}
K &= \frac{1}{\SpacDim - 1} X X^\trp = U \frac{S^2}{\SpacDim - 1} U^\trp \in \mathbb{R}^{\SampDim \stimes \SampDim} \label{eq:sigcovM}\\
K &= \frac{1}{\SpacDim - 1} \sum_{i=1}^{\SpacDim} \bm{x}_i \bm{x}_i^\trp \label{eq:sigcov}\\
K_{ij} &= \frac{1}{\SpacDim - 1} \feature{x}_i^\trp \feature{x}_j \label{eq:gramfea}
\end{align}
\noindent when the mean of the rows of $X$ was removed.
It is also the Gram matrix of the features (due to eq.~\eqref{eq:gramfea}).

\subsection{Principal components analysis}
PCA makes the covariance matrix diagonal.
Works the same for the two views, but the results are interpreted differently.

\subsubsection{Feature space view}

In the feature setup, after removing the column mean of $X$,
\begin{equation}
  \Sigma = \frac{1}{\SampDim - 1}\feature{X} \feature{X}^\trp = V \frac{S^2}{\SampDim - 1} V^\trp
\end{equation}
\noindent hence the vectors $\bset{\bm{v}_i}$ are the base in which the covariance is diagonal, and the eigenvalues of the covariance matrix are

\begin{equation}
\Lambda = \frac{S^2}{\SampDim - 1}
\end{equation}

In the decomposition
\begin{equation}
  \feature{X} = V P
\end{equation}

\noindent the matrix $P$ contains the scores of $\feature{X}$ in the basis $V$.


\subsubsection{Function space view}

In the signal setup, after removing the row mean of $X$,
\begin{equation}
  K = \frac{1}{\SpacDim - 1}X X^\trp = U \frac{S^2}{\SpacDim - 1} U^\trp
\end{equation}
\noindent hence the vectors $\bset{\bm{u}_i}$ are the base in which the covariance is diagonal, and the eigenvalues of the covariance matrix are

\begin{equation}
\Lambda = \frac{S^2}{\SpacDim - 1}
\end{equation}

In the decomposition
\begin{equation}
  X = U L
\end{equation}

\noindent the matrix $L$ contains the loadings of $X$ in the basis $U$.

\begin{align}
x_j(t) &= \sum_{k=1}^{K} \varphi_k(t) \ell_{kj} = \left(\varPhi(t) L\right)_{:j} \\
\varPhi(t) &= \begin{bmatrix} \varphi_1(t) & \ldots & \varphi_K(t) \end{bmatrix}\\
X_{ij} &= x_j(t_i), \quad i=1,\ldots, \SampDim\\
X &= \Phi L, \quad \Phi = \varPhi\left[\bset{t_i}\right]
\end{align}

\section{Proper Orthogonal Decomposition}

%\bibliographystyle{unsrtnat}
%\bibliography{references}

\end{document}
