{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PCA regression (PCR)\n",
    "\n",
    "    Copyright (C) 2019 Juan Pablo Carbajal\n",
    "\n",
    "    This program is free software; you can redistribute it and/or modify\n",
    "    it under the terms of the GNU General Public License as published by\n",
    "    the Free Software Foundation; either version 3 of the License, or\n",
    "    (at your option) any later version.\n",
    "\n",
    "    This program is distributed in the hope that it will be useful,\n",
    "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
    "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
    "    GNU General Public License for more details.\n",
    "\n",
    "    You should have received a copy of the GNU General Public License\n",
    "    along with this program. If not, see <http://www.gnu.org/licenses/>.\n",
    "\n",
    "    Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear regression recap\n",
    "\n",
    "\\begin{align}\n",
    "y_i &= x_{i1} \\beta_1 + \\ldots + x_{id} \\beta_d, \\quad \\frac{1}{n}\\sum_{i=1}^n x_{ik} = 0, \\; k=1,\\ldots,d\\\\\n",
    "\\mathbf{y} &= X\\mathbf{\\beta} + \\mathbf{\\epsilon}\\\\ \n",
    "\\mathbf{y} &\\in \\mathbb{R}^{n\\times 1}, \\; X \\in \\mathbb{R}^{n\\times d}, \\; \\mathbf{\\beta} \\in \\mathbb{R}^{d\\times 1} \\\\\n",
    "\\end{align}\n",
    "\n",
    "This are $n$ equations (as many as samples) in $d$ variables (input dimension). We can multiply both sides by the tranpose of $X$ (lets forget about the noise for a moment), i.e.\n",
    "\n",
    "\\begin{align}\n",
    "X^\\top \\mathbf{y} &\\simeq X^\\top X\\mathbf{\\beta}\\\\\n",
    "X^\\top X &\\in \\mathbb{R}^{d\\times d}\n",
    "\\end{align}\n",
    "\n",
    "The matrix $\\Sigma = X^\\top X$ is the covariance of the $n$ $d$-dimensional vectors $\\mathbf{x}_i = X_{i:}$. If this matrix is not singular; that is, its determinat is not zero, or equivalently $X$ is full column rank ($d$ linearly independent columns); then we can calculate $\\Sigma^{-1}$ and we get\n",
    "\n",
    "\\begin{equation}\n",
    "(X^\\top X)^{-1} X^\\top \\mathbf{y} = \\mathbf{\\beta}\n",
    "\\end{equation}\n",
    "\n",
    "This is the least-squares solution to the linear regression problem.\n",
    "\n",
    "## High dimensions\n",
    "\n",
    "If $n < d$, we have less samples than dimensions (i.e. less equations than unknowns) and $X$ cannot be full rank because\n",
    "\n",
    "\\begin{equation}\n",
    "\\operatorname{rank} X \\le \\operatorname{min}(n,d)\n",
    "\\end{equation}\n",
    "\n",
    "That is, we can have at most $n$ independent columns in X. One way to check the avialble rank is to perfrom PCA on the inputs. Using the singular value decomposition (SVD) of the data matrix we get\n",
    "\n",
    "\\begin{align}\n",
    "X &= U S V^\\top\\\\\n",
    "&U \\in \\mathbb{R}^{n\\times n},\\; S \\in \\mathbb{R}^{n\\times n},\\; V \\in \\mathbb{R}^{n\\times d}\\\\\n",
    "&U^\\top U = I, \\; V^\\top V = I\\\\\n",
    "X^\\top X &= (U S V^\\top)^\\top U S V^\\top = V S U^\\top U S V^\\top = V S^2 V^\\top\\\\\n",
    "\\end{align}\n",
    "\n",
    "and $S$ is a diagonal matrix. Its diagonal can be used to select a reduced set of dimensions. Lets see an example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib notebook\n",
    "\n",
    "n = 30\n",
    "d = 100\n",
    "# Build matrix with only rank 10\n",
    "xli = np.random.randn(d, 10)            # set 10 d-dimensional l.i. vectors\n",
    "M = np.random.randn(n, 10)              # Mixing matrix\n",
    "X = M @ xli.T                           # Assemble a matrix\n",
    "X -= np.mean(X, axis=0, keepdims=True)  # remove mean of columns\n",
    "\n",
    "# SVD decomposition\n",
    "U, S, VT = np.linalg.svd(X, full_matrices=False, compute_uv=True)\n",
    "print(\"U {}x{}, S {}, VT {}x{}\".format(*U.shape, *S.shape, *VT.shape))\n",
    "\n",
    "plt.figure(1)\n",
    "plt.clf()\n",
    "plt.semilogy(range(1,n+1), S / S[0], '-o')\n",
    "plt.grid()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example we created a $n$ by $d$ matrix $X$, with only $10$ linearly independent vectors (rank is $10$). So data matrix is reproduced with only $10$ components."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "VT10 = VT[:10,:]                 # The first 10 axis\n",
    "W10 = (U[:,:10] * S[:10])        # The first 10 components\n",
    "X10 = W10 @ VT10                 # Recovered matrix\n",
    "assert np.allclose(X, X10)       # Check element-wise similarity\n",
    "np.linalg.norm(X - X10)          # Check matrix norm of the difference"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we can now proceed to do the regression on the reduced matrix\n",
    "\n",
    "\\begin{align}\n",
    "\\mathbf{\\beta} = (X^\\top X)^{-1} X^\\top \\mathbf{y} = (V S^2 V^\\top)^{-1} V S U^\\top \\mathbf{y} = V S^{-2} V^\\top V S U^\\top \\mathbf{y} = V S^{-1} U^\\top \\mathbf{y} = V \\mathbf{\\gamma}\n",
    "\\end{align}\n",
    "\n",
    "Lets check this with an example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if 'beta' not in locals():\n",
    "    beta = 2 * np.random.rand(d) - 1 # random vectors in [-1,1]\n",
    "y = X @ beta\n",
    "gamma = np.diag(1 / S[:10]) @ U[:,:10].T @ y\n",
    "beta_pcr = VT10.T @ gamma\n",
    "y10 = X @ beta_pcr\n",
    "\n",
    "Xtst = np.random.randn(n, 10) @ xli.T         # Assemble test input matrix\n",
    "Xtst -= np.mean(Xtst, axis=0, keepdims=True)  # remove mean of columns\n",
    "ytst = Xtst @ beta\n",
    "y10tst = Xtst @ beta_pcr\n",
    "\n",
    "plt.figure()\n",
    "plt.clf()\n",
    "plt.plot(y10, y, 'or', label='Training')\n",
    "plt.plot(y10tst, ytst, 'og', label='Testing')\n",
    "plt.plot(plt.xlim(), plt.ylim(), '--k')\n",
    "plt.xlabel('PCR prediction');\n",
    "plt.ylabel('Training outputs');\n",
    "plt.legend();\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "plt.clf()\n",
    "ax = fig.add_subplot(121)\n",
    "ax.plot(range(1,101), beta_pcr, 'o', range(1,101), beta,'s');\n",
    "ax.set_xlabel('dimension')\n",
    "ax.set_ylabel('coefficient')\n",
    "ax.legend(['PCR', 'Generator'])\n",
    "\n",
    "ax = fig.add_subplot(122)\n",
    "ax.plot(range(1,11), gamma, 'o');\n",
    "ax.set_xlabel('dimension');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In applications however we do not know if the outputs $\\mathbf{y}$ actually depend on the principal vectors, as in our example.\n",
    "It could be that the output only depends on 2 of the components. How can we choose the principal compoenets for the regression?\n",
    "\n",
    "Luckly, the PCR step described above is equivalent at regressing each principal component independently (because the matrix $U$ is unitary). Lets check this"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gamma_w = []\n",
    "for u in W10.T:\n",
    "    gamma_w.append((np.linalg.pinv(u.reshape(-1,1)) @ y)[0])\n",
    "gamma_w = np.array(gamma_w)\n",
    "assert np.allclose(gamma_w, gamma)       # Check element-wise similarity\n",
    "print(np.linalg.norm(gamma_w - gamma))\n",
    "print(np.hstack([gamma.reshape(-1,1), gamma_w.reshape(-1,1)]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hence if we had an output that depends only on a few components, we can discover them by checking, e.g. the most correlated ones, or by using cross-validation to select the best subset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ysub = (W10 @ [0,0,-1,0,0,0,0,0,1,0]) + 0.8 * np.random.randn(n)  # Data depends on only 2 of the latent vectors\n",
    "gamma_sub = []\n",
    "for u in W10.T:\n",
    "    gamma_sub.append((np.linalg.pinv(u.reshape(-1,1)) @ ysub)[0])\n",
    "gamma_sub = np.array(gamma_sub)\n",
    "\n",
    "idx = np.argsort(np.abs(gamma_sub))[::-1]\n",
    "ysub_ = np.cumsum(W10[:,idx] * gamma_sub[idx], axis=1)\n",
    "rmse = np.sqrt(np.mean((ysub_ - ysub.reshape(-1,1))**2, axis=0))\n",
    "\n",
    "fig = plt.figure()\n",
    "plt.clf()\n",
    "fig.add_subplot(121)\n",
    "plt.plot(range(1,11), rmse,'-o')\n",
    "plt.xticks(range(1,11), idx)\n",
    "plt.xlabel('Selected component');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we use a scikit-learn to implement PCR, and repeat the results above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.decomposition import PCA\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from sklearn.model_selection import cross_val_score\n",
    "\n",
    "pca = PCA(n_components=10)\n",
    "pca.fit(X)\n",
    "W = X @ pca.components_.T\n",
    "\n",
    "reg = LinearRegression(fit_intercept=False)\n",
    "score = []\n",
    "score_std = []\n",
    "for i in range(10):\n",
    "    scores = cross_val_score(reg, W[:,i].reshape(-1,1), ysub, cv=2)\n",
    "    score.append(scores.mean())\n",
    "    score_std.append(scores.std())\n",
    "score = np.concatenate([np.array(score).reshape(-1,1), np.array(score_std).reshape(-1,1)], axis=1)\n",
    "\n",
    "idx = np.argsort(score[:,0])[::-1]\n",
    "print(idx)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "pcar_toolbox",
   "language": "python",
   "name": "pcar_toolbox"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
